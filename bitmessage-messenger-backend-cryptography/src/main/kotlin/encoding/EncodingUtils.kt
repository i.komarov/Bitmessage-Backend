package encoding

import base.Streamable
import statistics.AccessCounter
import statistics.AccessCounter.Companion.increment
import sun.reflect.generics.reflectiveObjects.NotImplementedException
import utils.base58_alphabet
import utils.base58_encodedZero
import utils.divmod
import java.io.ByteArrayOutputStream
import java.io.OutputStream
import java.nio.ByteBuffer
import java.util.*
import jdk.nashorn.tools.ShellFunctions.input



/**
 * Created by i_komarov on 04.07.17.
 */

/**
 * This file contains functions that handle messageEncoding simple types from byte streamNumber, according to
 * https://bitmessage.org/wiki/Protocol_specification#Common_structures
 */

fun int8(value: Long, output: OutputStream, counter: AccessCounter? = null) {
    output.write(value.toInt())
    counter?.let { increment(counter = counter, length = 1) }
}

fun int16(value: Long, output: OutputStream, counter: AccessCounter? = null) {
    output.write(ByteBuffer.allocate(2).putShort(value.toShort()).array())
    counter?.let { increment(counter = counter, length = 2) }
}

fun int16(value: Long, buffer: ByteBuffer) = buffer.putShort(value.toShort())

fun int32(value: Long, output: OutputStream, counter: AccessCounter? = null) {
    output.write(ByteBuffer.allocate(4).putInt(value.toInt()).array())
    counter?.let { increment(counter = counter, length = 4) }
}

fun int32(value: Long, buffer: ByteBuffer) = buffer.putInt(value.toInt())

fun int64(value: Long, output: OutputStream, counter: AccessCounter? = null) {
    output.write(ByteBuffer.allocate(8).putLong(value).array())
    counter?.let { increment(counter = counter, length = 8) }
}

fun int64(value: Long, buffer: ByteBuffer) = buffer.putLong(value)

fun varString(value: String, output: OutputStream) = with(value.toByteArray(charset("UTF-8"))) {
    varInt(value = size.toLong(), output = output)
    output.write(this)
}

fun varString(value: String, buffer: ByteBuffer) = with(value.toByteArray(charset("UTF-8"))) {
    buffer.put(varInt(value = size.toLong()))
    buffer.put(this)
}

fun varBytes(value: ByteArray, output: OutputStream) {
    varInt(value = value.size.toLong(), output = output)
    output.write(value)
}

fun varBytes(value: ByteArray, buffer: ByteBuffer) {
    varInt(value = value.size.toLong(), buffer = buffer)
    buffer.put(value)
}

fun varIntList(values: LongArray, output: OutputStream) {
    varInt(value = values.size.toLong(), output = output)
    values.forEach {
        varInt(value = it, output = output)
    }
}

fun varIntList(values: LongArray, buffer: ByteBuffer) {
    varInt(value = values.size.toLong(), buffer = buffer)
    values.forEach {
        varInt(value = it, buffer = buffer)
    }
}

fun varInt(value: Long): ByteArray = with(ByteBuffer.allocate(9)) {
    varInt(value = value, buffer = this)
    flip()
    utils.truncate(source = array(), size = limit())
}

fun varInt(value: Long, output: OutputStream, counter: AccessCounter? = null) = with(ByteBuffer.allocate(9)) {
    varInt(value = value, buffer = this)
    flip()
    output.write(array(), 0, limit())
    counter?.let {
        increment(counter = counter, length = limit())
    }
}

fun varInt(value: Long, buffer: ByteBuffer) = with(buffer) {
    when {
        value < 0 -> {
            put(0xFF.toByte())
            putLong(value)
        }
        value < 0xFD -> {
            put(value.toByte())
        }
        value <= 0xFFFFL -> {
            put(0xFD.toByte())
            putShort(value.toShort())
        }
        value <= 0xFFFFFFFFL -> {
            put(0xFE.toByte())
            putInt(value.toInt())
        }
        else -> {
            put(0xFF.toByte())
            putLong(value)
        }
    }
}

/**
 * Serializes a {@link Streamable} object and returns the byte array.
 *
 * @param streamable the object to be serialized
 * @return an array of bytes representing the given streamable object.
 */
fun bytes(streamable: Streamable): ByteArray = with(ByteArrayOutputStream()) {
    streamable.write(this)
    toByteArray()
}

/**
 * @param streamable the object to be serialized
 * @param padding    the result will be padded such that its length is a multiple of <em>padding</em>
 * @return the bytes of the given {@link Streamable} object, 0-padded such that the final length is x*padding.
 */
fun bytes(streamable: Streamable, padding: Int): ByteArray {
    val stream = ByteArrayOutputStream()
    streamable.write(stream)
    val offset = padding - stream.size().rem(padding)
    val length = stream.size() + offset
    return ByteArray(length).apply {
        stream.write(this, offset, length)
    }
}

fun base58_encode(data: ByteArray): String = when {
    data.isEmpty() -> ""
    else -> {
        //count leading zeros
        var zeros = 0
        while(zeros < data.size && data[zeros] == 0.toByte()) {
            ++zeros
        }
        // Convert base-256 digits to base-58 digits (plus conversion to ASCII characters)
        val input = Arrays.copyOf(data, data.size)
        val encoded: CharArray = kotlin.CharArray(input.size * 2)
        var outputStart = encoded.size
        var inputStart = zeros
        while (inputStart < input.size) {
            encoded[--outputStart] = base58_alphabet[divmod(input, inputStart, 256, 58).toInt()]
            if (input[inputStart] == 0.toByte()) {
                ++inputStart // optimization - skip leading zeros
            }
        }

        while(outputStart < encoded.size && encoded[outputStart] == base58_encodedZero) {
            ++outputStart
        }

        while(--zeros >= 0) {
            encoded[--outputStart] = base58_encodedZero
        }

        String(encoded, outputStart, encoded.size - outputStart)
    }
}

