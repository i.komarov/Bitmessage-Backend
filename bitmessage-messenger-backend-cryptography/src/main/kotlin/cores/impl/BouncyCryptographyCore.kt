package cores.impl

import cores.base.BasicCryptographyCore
import org.bouncycastle.asn1.x9.X9ECParameters
import org.bouncycastle.crypto.ec.CustomNamedCurves
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.bouncycastle.math.ec.ECPoint
import utils.keyToBigInteger
import java.math.BigInteger
import java.util.*

/**
 * Created by i_komarov on 05.07.17.
 */

class BouncyCryptographyCore: BasicCryptographyCore(provider) {

    companion object {
        val params: X9ECParameters by lazy { CustomNamedCurves.getByName("secp256k1") }

        val provider: BouncyCastleProvider by lazy { BouncyCastleProvider() }
    }

    override fun multiplyEllipticCurvePoints(x: ByteArray, y: ByteArray): ByteArray = keyToEllipticCurvePoint(x)
            .multiply(keyToBigInteger(y))
            .normalize()
            .getEncoded(false)

    override fun generateEllipticCurvePoint(x: ByteArray, y: ByteArray): ByteArray = params.curve.createPoint(
            BigInteger(1, x),
            BigInteger(1, y)
    ).getEncoded(false)

    private fun keyToEllipticCurvePoint(publicKey: ByteArray): ECPoint = params.curve.createPoint(
            BigInteger(1, Arrays.copyOfRange(publicKey, 1, 33)),
            BigInteger(1, Arrays.copyOfRange(publicKey, 33, 65))
    )
}