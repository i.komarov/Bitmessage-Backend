package cores.base

import entities.keys.PublicKey

/**
 * Created by i_komarov on 05.07.17.
 */
interface ICryptographyCore {

    /**
     * A helper method to calculate SHA-1 hashes. Supplying multiple byte arrays has the same result as a
     * concatenation of all arrays, but might perform better.
     * <p>
     * Please note that a new {@link MessageDigest} object is created at
     * each call (to ensure thread safety), so you shouldn't use this if you need to do many hash calculations in short
     * order on the same thread.
     * </p>
     *
     * @param data to get hashed
     * @return SHA hash of data
     */
    fun sha1(vararg data: ByteArray): ByteArray

    /**
     * A helper method to calculate SHA-256 hashes. This method allows to only use a part of the available bytes
     * to use for the hash calculation.
     * <p>
     * Please note that a new {@link MessageDigest} object is created at
     * each call (to ensure thread safety), so you shouldn't use this if you need to do many hash calculations in short
     * order on the same thread.
     * </p>
     *
     * @param data   to get hashed
     * @param length number of bytes to be taken into account
     * @return SHA-256 hash of data
     */
    fun sha256(data: ByteArray, length: Int): ByteArray

    /**
     * A helper method to calculate double SHA-256 hashes. This method allows to only use a part of the available bytes
     * to use for the hash calculation.
     * <p>
     * Please note that a new {@link MessageDigest} object is created at
     * each call (to ensure thread safety), so you shouldn't use this if you need to do many hash calculations in short
     * order on the same thread.
     * </p>
     *
     * @param data   to get hashed
     * @param length number of bytes to be taken into account
     * @return SHA-256 hash of data
     */
    fun doubleSha256(data: ByteArray, length: Int): ByteArray

    /**
     * A helper method to calculate SHA-512 hashes. Please note that a new {@link MessageDigest} object is created at
     * each call (to ensure thread safety), so you shouldn't use this if you need to do many hash calculations in
     * success on the same thread.
     *
     * @param data   to get hashed
     * @param offset of the data to be hashed
     * @param length of the data to be hashed
     * @return SHA-512 hash of data within the given range
     */
    fun sha512(data: ByteArray, offset: Int, length: Int): ByteArray

    /**
     * A helper method to calculate SHA-512 hashes. Please note that a new {@link MessageDigest} object is created at
     * each call (to ensure thread safety), so you shouldn't use this if you need to do many hash calculations in
     * success on the same thread.
     *
     * @param data to get hashed
     * @return SHA-512 hash of data
     */
    fun sha512(vararg data: ByteArray): ByteArray

    /**
     * A helper method to calculate double SHA-512 hashes. This method allows to only use a part of the available bytes
     * to use for the hash calculation.
     * <p>
     * Please note that a new {@link MessageDigest} object is created at each call (to ensure thread safety), so you
     * shouldn't use this if you need to do many hash calculations in short order on the same thread.
     * </p>
     *
     * @param data   to get hashed
     * @param length number of bytes to be taken into account
     * @return SHA-512 hash of data
     */
    fun doubleSha512(data: ByteArray, length: Int): ByteArray

    /**
     * A helper method to calculate doubleSHA-512 hashes. Please note that a new {@link MessageDigest} object is created
     * at each call (to ensure thread safety), so you shouldn't use this if you need to do many hash calculations in
     * success on the same thread.
     *
     * @param data to get hashed
     * @return SHA-512 hash of data
     */
    fun doubleSha512(vararg data: ByteArray): ByteArray

    /**
     * A helper method to calculate RIPEMD-160 hashes. Supplying multiple byte arrays has the same result as a
     * concatenation of all arrays, but might perform better.
     * <p>
     * Please note that a new {@link MessageDigest} object is created at
     * each call (to ensure thread safety), so you shouldn't use this if you need to do many hash calculations in short
     * order on the same thread.
     * </p>
     *
     * @param data to get hashed
     * @return RIPEMD-160 hash of data
     */
    fun ripemd160(vararg data: ByteArray): ByteArray

    /**
     * Calculates the MAC for a entities.messages (data)
     *
     * @param keyMac the symmetric key used
     * @param data  the entities.messages data to calculate the MAC for
     * @return the MAC
     */
    fun mac(keyMac: ByteArray, data: ByteArray): ByteArray

    /**
     * @param length number of bytes to return
     * @return an array of the given size containing random bytes
     */
    fun randomBytes(length: Int): ByteArray

    /**
     * @return a random number of type long
     */
    fun randomNonce(): Long

    fun generateRipe(publicKey: PublicKey): ByteArray

    fun generateRipe(publicSigningKey: ByteArray, publicEncryptionKey: ByteArray): ByteArray

    fun multiplyEllipticCurvePoints(x: ByteArray, y: ByteArray): ByteArray

    fun generateEllipticCurvePoint(x: ByteArray, y: ByteArray): ByteArray

    fun generateTag(version: Long, streamNumber: Long, ripe: ByteArray): ByteArray

    fun generateInventoryVector(nonce: ByteArray, payloadWithoutNonce: ByteArray): ByteArray
}