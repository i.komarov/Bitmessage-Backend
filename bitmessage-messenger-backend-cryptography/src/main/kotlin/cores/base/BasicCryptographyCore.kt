package cores.base

import entities.keys.PublicKey
import utils.truncate
import java.io.ByteArrayOutputStream
import java.security.MessageDigest
import java.security.Provider
import java.security.SecureRandom
import java.util.*
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

/**
 * Created by i_komarov on 05.07.17.
 */
abstract class BasicCryptographyCore(private val provider: Provider): ICryptographyCore {

    private val random: SecureRandom by lazy { SecureRandom() }

    override fun sha1(vararg data: ByteArray): ByteArray = hash(provider, "SHA-1", data)

    override fun sha256(data: ByteArray, length: Int): ByteArray = with(MessageDigest.getInstance("SHA-256", provider)) {
        update(data, 0, length)
        digest()
    }

    override fun doubleSha256(data: ByteArray, length: Int): ByteArray = with(MessageDigest.getInstance("SHA-256", provider)) {
        update(data, 0, length)
        digest(digest())
    }

    override fun sha512(data: ByteArray, offset: Int, length: Int): ByteArray = with(MessageDigest.getInstance("SHA-512", provider)) {
        update(data, offset, length)
        digest()
    }

    override fun sha512(vararg data: ByteArray): ByteArray = hash(provider, "SHA-512", data)

    override fun doubleSha512(data: ByteArray, length: Int): ByteArray = with(MessageDigest.getInstance("SHA-512", provider)) {
        update(data, 0, length)
        digest(digest())
    }

    override fun doubleSha512(vararg data: ByteArray): ByteArray = with(MessageDigest.getInstance("SHA-512", provider)) {
        data.forEach { this.update(it) }
        digest(digest())
    }

    override fun ripemd160(vararg data: ByteArray): ByteArray = hash(provider, "RIPEMD160", data)


    override fun mac(keyMac: ByteArray, data: ByteArray): ByteArray = with(Mac.getInstance("HmacSHA256", provider)) {
        init(SecretKeySpec(keyMac, "HmacSHA256"))
        doFinal(data)
    }

    override fun randomBytes(length: Int): ByteArray = with(ByteArray(length)) {
        random.nextBytes(this)
        this
    }

    override fun randomNonce(): Long = random.nextLong()

    override fun generateRipe(publicKey: PublicKey): ByteArray = generateRipe(publicKey.signingKey(), publicKey.encryptionKey())

    override fun generateRipe(publicSigningKey: ByteArray, publicEncryptionKey: ByteArray): ByteArray = ripemd160(sha512(publicSigningKey, publicEncryptionKey))

    override fun generateTag(version: Long, streamNumber: Long, ripe: ByteArray): ByteArray = with(ByteArrayOutputStream()) {
        encoding.varInt(version, this)
        encoding.varInt(streamNumber, this)
        write(ripe)
        return Arrays.copyOfRange(doubleSha512(toByteArray()), 32, 64)
    }

    override fun generateInventoryVector(nonce: ByteArray, payloadWithoutNonce: ByteArray): ByteArray = truncate(
            source = doubleSha512(nonce, payloadWithoutNonce),
            size = 32
    )

    private fun hash(provider: Provider, algorithm: String, data: Array<out ByteArray>): ByteArray = with(MessageDigest.getInstance(algorithm, provider)) {
        data.forEach { this.update(it) }
        digest()
    }
}