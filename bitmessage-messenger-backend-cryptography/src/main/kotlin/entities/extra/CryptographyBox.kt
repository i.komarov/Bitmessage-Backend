package entities.extra

import base.Streamable
import cores.base.ICryptographyCore
import engine.base.ICryptographyEngine
import entities.keys.PrivateKey
import statistics.AccessCounter
import utils.componentX
import utils.componentY
import utils.leadingZeros
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.io.OutputStream
import java.nio.ByteBuffer
import java.util.*

/**
 * Created by i_komarov on 06.07.17.
 */
class CryptographyBox private constructor(
        private var initializationVector: ByteArray,
        private var curveType: Int,
        private var publicECKey: ByteArray,
        private var mac: ByteArray,
        private var encrypted: ByteArray) : Streamable {

    companion object {
        val serialVersionUID: Long = 7217659539975573852L
    }

    override fun write(output: OutputStream): Unit = with(output) {
        write(initializationVector)
        encoding.int16(curveType.toLong(), this)
        writeComponent(this, componentX(publicECKey))
        writeComponent(this, componentY(publicECKey))
        write(encrypted)
        write(mac)
    }

    override fun write(output: ByteBuffer): Unit = with(output) {
        put(initializationVector)
        encoding.int16(curveType.toLong(), this)
        writeComponent(this, componentX(publicECKey))
        writeComponent(this, componentY(publicECKey))
        put(encrypted)
        put(mac)
    }

    private fun writeComponent(output: OutputStream, component: ByteArray): Unit = with(output) {
        val offset = leadingZeros(component)
        val length = component.size - offset
        encoding.int16(length.toLong(), this)
        write(component, offset, length)
    }

    private fun writeComponent(buffer: ByteBuffer, component: ByteArray): Unit = with(buffer) {
        val offset = leadingZeros(component)
        val length = component.size - offset
        encoding.int16(length.toLong(), this)
        put(component, offset, length)
    }

    class Factory(private val core: ICryptographyCore, private val engine: ICryptographyEngine) {

        companion object {
            val defaultCurveType: Int = 0x02CA
        }

        fun create(data: Streamable, destinationPublicKey: ByteArray): CryptographyBox = create(
                encoding.bytes(data), destinationPublicKey
        )

        fun create(input: InputStream, length: Int): CryptographyBox = with(AccessCounter()) {
            CryptographyBox(
                    initializationVector = decoding.bytes(input, 16, this),
                    curveType = decoding.uint16(input, this),
                    publicECKey = core.generateEllipticCurvePoint(decoding.shortVarBytes(input, this), decoding.shortVarBytes(input, this)),
                    encrypted = decoding.bytes(input, length - this.length - 32),
                    mac = decoding.bytes(input, 32)
            )
        }

        fun create(data: ByteArray, destinationPublicKey: ByteArray): CryptographyBox {
            val initializationVector = core.randomBytes(16)
            val privateECKey: ByteArray = core.randomBytes(PrivateKey.privateKeySize)
            val publicECKey = engine.generatePublicKeyBytes(privateECKey)
            val publicKey = core.multiplyEllipticCurvePoints(destinationPublicKey, privateECKey)
            val publicKeyComponentX = componentX(publicKey)
            val hash = core.sha512(publicKeyComponentX)
            val keyE = Arrays.copyOfRange(hash, 0, 32)
            val keyM = Arrays.copyOfRange(hash, 32, 64)
            val encrypted = engine.encrypt(data, keyE, initializationVector)
            val mac = generateMac(initializationVector, defaultCurveType, publicECKey, encrypted, keyM)
            return CryptographyBox(initializationVector, defaultCurveType, publicECKey, mac, encrypted)
        }

        private fun generateMac(initializationVector: ByteArray, curveType: Int, publicECKey: ByteArray, encrypted: ByteArray, keyM: ByteArray): ByteArray = with(ByteArrayOutputStream()) {
            writeWithoutMac(initializationVector, curveType, publicECKey, encrypted, this)
            core.mac(keyM, toByteArray())
        }

        private fun writeWithoutMac(initializationVector: ByteArray, curveType: Int, publicECKey: ByteArray, encrypted: ByteArray, output: OutputStream) = with(output) {
            write(initializationVector)
            encoding.int16(curveType.toLong(), this)
            writeComponent(this, componentX(publicECKey))
            writeComponent(this, componentY(publicECKey))
            write(encrypted)
        }

        private fun writeComponent(output: OutputStream, component: ByteArray): Unit = with(output) {
            val offset = leadingZeros(component)
            val length = component.size - offset
            encoding.int16(length.toLong(), this)
            write(component, offset, length)
        }

        private fun writeComponent(buffer: ByteBuffer, component: ByteArray): Unit = with(buffer) {
            val offset = leadingZeros(component)
            val length = component.size - offset
            encoding.int16(length.toLong(), this)
            put(component, offset, length)
        }
    }

    class Manager(private val core: ICryptographyCore, private val engine: ICryptographyEngine) {

        fun decrypt(box: CryptographyBox, privateKey: ByteArray): InputStream {
            val publicKey: ByteArray = core.multiplyEllipticCurvePoints(box.publicECKey, privateKey)
            val hash: ByteArray = core.sha512(Arrays.copyOfRange(publicKey, 1, 33))
            val keyE = Arrays.copyOfRange(hash, 0, 32)
            val keyM = Arrays.copyOfRange(hash, 32, 64)
            if(!Arrays.equals(box.mac, generateMac(box.initializationVector, box.curveType, box.publicECKey, box.encrypted, keyM))) throw IllegalStateException("decryption failed: existing mac and calculated are not equal")
            return ByteArrayInputStream(engine.decrypt(box.encrypted, keyE, box.initializationVector))
        }

        private fun generateMac(initializationVector: ByteArray, curveType: Int, publicECKey: ByteArray, encrypted: ByteArray, keyM: ByteArray): ByteArray = with(ByteArrayOutputStream()) {
            writeWithoutMac(initializationVector, curveType, publicECKey, encrypted, this)
            core.mac(keyM, toByteArray())
        }

        private fun writeWithoutMac(initializationVector: ByteArray, curveType: Int, publicECKey: ByteArray, encrypted: ByteArray, output: OutputStream) = with(output) {
            write(initializationVector)
            encoding.int16(curveType.toLong(), this)
            writeComponent(this, componentX(publicECKey))
            writeComponent(this, componentY(publicECKey))
            write(encrypted)
        }

        private fun writeComponent(output: OutputStream, component: ByteArray): Unit = with(output) {
            val offset = leadingZeros(component)
            val length = component.size - offset
            encoding.int16(length.toLong(), this)
            write(component, offset, length)
        }

        private fun writeComponent(buffer: ByteBuffer, component: ByteArray): Unit = with(buffer) {
            val offset = leadingZeros(component)
            val length = component.size - offset
            encoding.int16(length.toLong(), this)
            put(component, offset, length)
        }
    }
}