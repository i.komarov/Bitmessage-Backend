package entities.extra

import base.Streamable
import java.io.OutputStream
import java.io.Serializable
import java.nio.ByteBuffer
import java.util.*

/**
 * Created by i_komarov on 11.07.17.
 */
class InventoryVector(val hash: ByteArray): Streamable, Serializable {

    companion object {
        val serialVersionUID: Long = -7349009673063348719L

        val default by lazy { InventoryVector(ByteArray(0)) }
    }

    override fun write(output: OutputStream): Unit {
        output.write(hash)
    }

    override fun write(output: ByteBuffer): Unit {
        output.put(hash)
    }

    override fun toString(): String = utils.toHexString(hash).toString()

    override fun equals(other: Any?): Boolean = when {
        this == other -> true
        other == null || javaClass != other.javaClass -> false
        else -> {
            other as InventoryVector
            Arrays.equals(hash, other.hash)
        }
    }

    override fun hashCode(): Int = Arrays.hashCode(hash)
}