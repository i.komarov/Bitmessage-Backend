package entities.labels

/**
 * Created by i_komarov on 11.07.17.
 */
enum class LabelType {
    INBOX     ,
    BROADCAST ,
    DRAFT     ,
    OUTBOX    ,
    SENT      ,
    UNREAD    ,
    TRASH
}