package entities.labels

import java.util.*

/**
 * Created by i_komarov on 11.07.17.
 */
class Label(
        var id: Any? = null,
        var label: String,
        var type: LabelType,
        var color: Int
) {

    companion object {
        val serialVersionUID: Long = 831782893630994914L
    }

    override fun toString(): String = label

    override fun equals(other: Any?): Boolean = when {
        this == other -> true
        other == null || javaClass != other.javaClass -> false
        else -> {
            other as Label
            Objects.equals(label, other.label)
        }
    }

    override fun hashCode(): Int = Objects.hash(label)
}