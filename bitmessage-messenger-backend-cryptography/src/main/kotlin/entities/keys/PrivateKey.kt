package entities.keys

import base.Streamable
import cores.base.ICryptographyCore
import engine.base.ICryptographyEngine
import entities.messages.base.Feature
import java.io.OutputStream
import java.nio.ByteBuffer
import java.io.ByteArrayOutputStream


/**
 * Created by i_komarov on 05.07.17.
 */
class PrivateKey(
        val privateSigningKey: ByteArray,
        val privateEncryptionKey: ByteArray,
        val publicKey: PublicKey
) : Streamable {

    companion object {
        val serialVersionUID: Long = 8562555470709110558L
        val privateKeySize: Int = 32
    }

    override fun write(output: OutputStream) {
        encoding.varInt(publicKey.version(), output)
        encoding.varInt(publicKey.streamNumber(), output)
        with(ByteArrayOutputStream()) {
            publicKey.writeNotEncrypted(this)
            encoding.varInt(this.size().toLong(), output)
            output.write(this.toByteArray())
            encoding.varInt(privateSigningKey.size.toLong(), output)
            output.write(privateSigningKey)
            encoding.varInt(privateEncryptionKey.size.toLong(), output)
            output.write(privateEncryptionKey)
        }
    }

    override fun write(output: ByteBuffer) {
        encoding.varInt(publicKey.version(), output)
        encoding.varInt(publicKey.streamNumber(), output)
        with(ByteArrayOutputStream()) {
            publicKey.writeNotEncrypted(this)
            encoding.varBytes(this.toByteArray(), output)
            encoding.varBytes(privateSigningKey, output)
            encoding.varBytes(privateEncryptionKey, output)
        }
    }

    class Factory(private val core: ICryptographyCore, private val engine: ICryptographyEngine, private val keysFactory: entities.keys.Factory) {

        fun determenistic(passphrase: String, numberOfAddresses: Int, version: Long, streamNumber: Long, shorter: Boolean, startNonce: Long = 0L): List<PrivateKey> {
            var nextNonce = startNonce
            return List(numberOfAddresses, {
                val pair = create(passphrase, version, streamNumber, shorter, nextNonce)
                nextNonce = pair.second
                pair.first
            })
        }

        fun create(shorter: Boolean, streamNumber: Long, nonceTrialsPerByte: Long, extraBytes: Long, vararg features: Feature): PrivateKey {
            var signingKey: ByteArray = engine.generatePublicKeyBytes(core.randomBytes(privateKeySize))
            var encryptionKey: ByteArray = engine.generatePublicKeyBytes(core.randomBytes(privateKeySize))
            var ripe: ByteArray = core.generateRipe(signingKey, encryptionKey)
            while(ripe[0] != 0.toByte() || (shorter && ripe[1] != 1.toByte())) {
                signingKey = engine.generatePublicKeyBytes(core.randomBytes(privateKeySize))
                encryptionKey = engine.generatePublicKeyBytes(core.randomBytes(privateKeySize))
                ripe = core.generateRipe(signingKey, encryptionKey)
            }

            val privateSigningKey = signingKey
            val privateEncryptionKey = encryptionKey
            val publicKey = engine.generatePublicKey(
                    PublicKey.latestVersion, streamNumber, privateSigningKey, privateEncryptionKey,
                    nonceTrialsPerByte, extraBytes, *features
            )

            return PrivateKey(privateSigningKey, privateEncryptionKey, publicKey)
        }

        fun create(passphrase: String, version: Long, streamNumber: Long): PrivateKey = create(passphrase, version, streamNumber, false, 0L).first

        private fun create(passphrase: String, version: Long, streamNumber: Long, shorter: Boolean = false, nextNonce: Long = 0L): Pair<PrivateKey, Long> {
            val seed = passphrase.toByteArray(charset("UTF-8"))
            var signingKeyNonce = nextNonce
            var encryptionKeyNonce = nextNonce + 1L
            var privateEncryptionKey: ByteArray
            var privateSigningKey: ByteArray
            var publicEncryptionKey: ByteArray
            var publicSigningKey: ByteArray
            var ripe: ByteArray
            do {
                privateEncryptionKey = utils.truncate(core.sha512(seed, encoding.varInt(encryptionKeyNonce)), 32)
                privateSigningKey = utils.truncate(core.sha512(seed, encoding.varInt(signingKeyNonce)), 32)
                publicEncryptionKey = engine.generatePublicKeyBytes(privateEncryptionKey)
                publicSigningKey = engine.generatePublicKeyBytes(privateSigningKey)
                ripe = core.ripemd160(core.sha512(publicSigningKey, publicEncryptionKey))

                signingKeyNonce += 2L
                encryptionKeyNonce += 2L
            } while (ripe[0] != 0.toByte() || (shorter && ripe[1] != 0.toByte()))

            return Pair(
                    PrivateKey(privateSigningKey, privateEncryptionKey, keysFactory.generatePublicKey(version, streamNumber, publicSigningKey, publicEncryptionKey, 1000, 1000)),
                    signingKeyNonce
            )
        }
    }
}