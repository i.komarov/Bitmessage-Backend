package entities.keys

import java.io.InputStream
import java.io.OutputStream
import java.util.*

/**
 * Created by i_komarov on 05.07.17.
 */
/**
 * A version 3 public key.
 */
class PublicKeyV3(
        version: Long,
        streamNumber: Long,
        behaviorBitField: Int,
        publicSigningKey: ByteArray,
        publicEncryptionKey: ByteArray,
        val nonceTrialsPerByte: Long,
        val extraBytes: Long,
        var signature: ByteArray?
) : PublicKeyV2(version, streamNumber, behaviorBitField, publicSigningKey, publicEncryptionKey) {

    companion object {
        val serialVersionUID: Long = 6958853116648528319L
    }

    override fun version(): Long = 3L

    fun nonceTrialsPerByte(): Long = nonceTrialsPerByte

    override fun extraBytes(): Long = extraBytes

    override fun write(output: OutputStream): Unit = with(output) {
        sign(this)
        encoding.varInt(signature!!.size.toLong(), this)
        write(signature)
    }

    override fun sign(output: OutputStream): Unit = with(output) {
        super.write(this)
        encoding.varInt(nonceTrialsPerByte, this)
        encoding.varInt(extraBytes, this)
    }

    override fun signature(): ByteArray? = signature

    override fun signature(signature: ByteArray?) {
        this.signature = signature
    }

    override fun equals(other: Any?): Boolean = when {
        this == other -> true
        other == null || this::class != other::class -> false
        else -> {
            other as PublicKeyV3
            Objects.equals(nonceTrialsPerByte, other.nonceTrialsPerByte) &&
                    Objects.equals(extraBytes, other.extraBytes) &&
                    stream == other.stream &&
                    behaviorBitField == other.behaviorBitField &&
                    Arrays.equals(publicSigningKey, other.publicSigningKey) &&
                    Arrays.equals(publicEncryptionKey, other.publicEncryptionKey)
        }
    }

    override fun hashCode(): Int = Objects.hash(nonceTrialsPerByte, extraBytes)

    open class Factory: PublicKeyV2.Factory() {

        override fun create(input: InputStream, streamNumber: Long): PublicKeyV3 = create(
                version = 3L,
                streamNumber = streamNumber,
                behaviorBitField = decoding.uint32(input).toInt(),
                publicSigningKey = decoding.bytes(input, 64),
                publicEncryptionKey = decoding.bytes(input, 64),
                nonceTrialsPerByte = decoding.varInt(input),
                extraBytes = decoding.varInt(input),
                signature = decoding.varBytes(input)
        )

        open fun create(
                version: Long, streamNumber: Long, behaviorBitField: Int,
                publicSigningKey: ByteArray, publicEncryptionKey: ByteArray, nonceTrialsPerByte: Long,
                extraBytes: Long, signature: ByteArray
        ): PublicKeyV3 = PublicKeyV3(
                version = version,
                streamNumber = streamNumber,
                behaviorBitField = behaviorBitField,
                publicSigningKey = publicSigningKey,
                publicEncryptionKey = publicEncryptionKey,
                nonceTrialsPerByte = nonceTrialsPerByte,
                extraBytes = extraBytes,
                signature = signature
        )
    }
}
