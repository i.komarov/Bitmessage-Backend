package entities.keys

import entities.messages.base.ObjectPayload
import java.io.OutputStream
import java.nio.ByteBuffer

/**
 * Created by i_komarov on 05.07.17.
 */
abstract class PublicKey(version: Long): ObjectPayload(version = version) {

    companion object {
        val serialVersionUID: Long = -6634533361454999619L
        val latestVersion: Long = 4L
    }

    abstract fun signingKey(): ByteArray

    abstract fun encryptionKey(): ByteArray

    abstract fun behaviorBitField(): Int

    open fun nonceTrialsPerBytes(): Long = 0L

    open fun extraBytes(): Long = 0L

    open fun writeNotEncrypted(output: OutputStream): Unit = write(output)

    open fun writeNotEncrypted(buffer: ByteBuffer): Unit = write(buffer)

    protected fun append0x04(key: ByteArray): ByteArray = when(key.size) {
        65 -> key
        else -> with(ByteArray(65)) {
            this[0] = 4.toByte()
            System.arraycopy(key, 0, this, 1, 64)
            this
        }
    }
}
