package entities.keys

import cores.base.ICryptographyCore
import entities.encryption.IDecryptionVisitor
import entities.encryption.IEncryptableElement
import entities.encryption.IEncryptionVisitor
import entities.extra.CryptographyBox
import entities.messages.base.ObjectType
import java.io.InputStream
import java.io.OutputStream
import java.nio.ByteBuffer
import java.util.*

/**
 * Created by i_komarov on 05.07.17.
 */

class PublicKeyV4 private constructor(
        version: Long,
        val streamNumber: Long,
        val tag: ByteArray,
        var decrypted: PublicKeyV3? = null,
        var encrypted: CryptographyBox? = null
): PublicKey(version), IEncryptableElement {

    companion object {
        val serialVersionUID: Long = 1556710353694033093L
    }

    override fun type(): ObjectType = ObjectType.PUBLIC_KEY

    override fun streamNumber(): Long = streamNumber

    override fun version(): Long = 4L

    override fun signingKey(): ByteArray = decrypted?.signingKey() ?: ByteArray(0)

    override fun encryptionKey(): ByteArray = decrypted?.encryptionKey() ?: ByteArray(0)

    override fun behaviorBitField(): Int = decrypted?.behaviorBitField()!!

    override fun signature(): ByteArray? = decrypted?.signature

    override fun signature(signature: ByteArray?) {
        decrypted?.signature(signature)
    }

    override fun isSigned(): Boolean = true

    override fun nonceTrialsPerBytes(): Long = decrypted!!.nonceTrialsPerBytes()

    override fun extraBytes(): Long = decrypted!!.extraBytes

    override fun write(output: OutputStream) {
        output.write(tag)
        encrypted?.write(output)
    }

    override fun write(output: ByteBuffer) {
        output.put(tag)
        encrypted?.write(output)
    }

    override fun writeNotEncrypted(output: OutputStream) {
        output.write(tag)
        decrypted?.write(output)
    }

    override fun writeNotEncrypted(buffer: ByteBuffer) {
        buffer.put(tag)
        decrypted?.write(buffer)
    }

    override fun sign(output: OutputStream) {
        output.write(tag)
        decrypted?.sign(output)
    }

    override fun isDecrypted(): Boolean = decrypted != null

    override fun acceptEncryptionVisitor(visitor: IEncryptionVisitor, publicKey: ByteArray) {
        visitor.visit(this, publicKey)
    }

    override fun acceptDecryptionVisitor(visitor: IDecryptionVisitor, privateKey: ByteArray) {
        visitor.visit(this, privateKey)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false

        other as PublicKeyV4

        if (streamNumber != other.streamNumber) return false
        if (!Arrays.equals(tag, other.tag)) return false
        return !if (decrypted != null) decrypted!! != other.decrypted else other.decrypted != null

    }

    override fun hashCode(): Int {
        var result = (streamNumber xor streamNumber.ushr(32)).toInt()
        result = 31 * result + Arrays.hashCode(tag)
        result = 31 * result + if (decrypted != null) decrypted!!.hashCode() else 0
        return result
    }

    open class Factory(private val core: ICryptographyCore, private val boxFactory: CryptographyBox.Factory): PublicKeyV3.Factory() {

        open fun create(decrypted: PublicKeyV3): PublicKeyV4 {
            val tag = core.generateTag(4L, decrypted.streamNumber(), core.generateRipe(decrypted.signingKey(), decrypted.encryptionKey()))
            return PublicKeyV4(
                    version = 4L,
                    streamNumber = decrypted.streamNumber(),
                    tag =  tag,
                    decrypted = decrypted
            )
        }

        open fun create(streamNumber: Long, tag: ByteArray, encrypted: CryptographyBox): PublicKeyV4 {
            return PublicKeyV4(
                    version = 4L,
                    streamNumber = streamNumber,
                    tag = tag,
                    encrypted = encrypted
            )
        }

        open fun create(stream: InputStream, streamNumber: Long, length: Long, encrypted: Boolean): PublicKeyV4 = when {
            encrypted -> PublicKeyV4(
                    version = 4L,
                    streamNumber = streamNumber,
                    tag = decoding.bytes(stream, 32),
                    encrypted = boxFactory.create(stream, (length - 32).toInt())
            )
            else -> create(create(stream, streamNumber))
        }
    }
}