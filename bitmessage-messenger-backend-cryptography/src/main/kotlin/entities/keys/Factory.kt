package entities.keys

import cores.base.ICryptographyCore
import engine.base.ICryptographyEngine
import entities.extra.CryptographyBox
import entities.messages.base.Feature

/**
 * Created by i_komarov on 05.07.17.
 */

class Factory(core: ICryptographyCore, engine: ICryptographyEngine) {

    private val factoryV2 = PublicKeyV2.Factory()
    private val factoryV3 = PublicKeyV3.Factory()
    private val factoryV4 = PublicKeyV4.Factory(core, CryptographyBox.Factory(core, engine))

    fun generatePublicKey(version: Long, streamNumber: Long, publicSigningKey: ByteArray, publicEncryptionKey: ByteArray,
                          nonceTrialsPerByte: Long, extraBytes: Long, vararg features: Feature): PublicKey = generatePublicKey(
            version, streamNumber, publicSigningKey, publicEncryptionKey,
            nonceTrialsPerByte, extraBytes, Feature.bitField(*features)
    )

    fun generatePublicKey(version: Long, streamNumber: Long, publicSigningKey: ByteArray, publicEncryptionKey: ByteArray,
            nonceTrialsPerByte: Long, extraBytes: Long, behaviorBitField: Int): PublicKey = when(version) {
        2L -> factoryV2.create(
                version = version,
                streamNumber = streamNumber,
                behaviorBitField = behaviorBitField,
                publicSigningKey = publicSigningKey,
                publicEncryptionKey = publicEncryptionKey
        )
        3L -> factoryV3.create(
                version = version,
                streamNumber = streamNumber,
                behaviorBitField = behaviorBitField,
                publicSigningKey = publicSigningKey,
                publicEncryptionKey = publicEncryptionKey,
                nonceTrialsPerByte = nonceTrialsPerByte,
                extraBytes = extraBytes,
                signature = ByteArray(0)
        )
        4L -> factoryV4.create(
                decrypted = factoryV3.create(
                        version = version,
                        streamNumber = streamNumber,
                        behaviorBitField = behaviorBitField,
                        publicSigningKey = publicSigningKey,
                        publicEncryptionKey = publicEncryptionKey,
                        nonceTrialsPerByte = nonceTrialsPerByte,
                        extraBytes = extraBytes,
                        signature = ByteArray(0)
                )
        )
        else -> throw IllegalStateException("Unexpected public key version ${version}")
    }
}