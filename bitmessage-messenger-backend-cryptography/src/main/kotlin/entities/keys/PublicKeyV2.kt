package entities.keys

import entities.messages.base.ObjectType
import java.io.InputStream
import java.io.OutputStream
import java.nio.ByteBuffer

/**
 * Created by i_komarov on 05.07.17.
 */
/**
 * A version 2 public key.
 */
open class PublicKeyV2(version: Long, streamNumber: Long, protected var behaviorBitField: Int, publicSigningKey: ByteArray, publicEncryptionKey: ByteArray) : PublicKey(version) {

    companion object {
        val serialVersionUID: Long = -257598690676510460L
    }

    protected var stream: Long = streamNumber
    protected var publicSigningKey: ByteArray
    protected var publicEncryptionKey: ByteArray

    init {
        this.publicSigningKey = append0x04(publicSigningKey)
        this.publicEncryptionKey = append0x04(publicEncryptionKey)
    }

    override fun write(output: OutputStream): Unit = with(output) {
        encoding.int32(behaviorBitField.toLong(), this)
        write(publicSigningKey, 1, 64)
        write(publicEncryptionKey, 1, 64)
    }

    override fun write(output: ByteBuffer): Unit = with(output) {
        encoding.int32(behaviorBitField.toLong(), this)
        put(publicSigningKey, 1, 64)
        put(publicEncryptionKey, 1, 64)
    }

    override fun version(): Long = 2L

    override fun type(): ObjectType = ObjectType.PUBLIC_KEY

    override fun streamNumber(): Long = stream

    override fun signingKey(): ByteArray = publicSigningKey

    override fun encryptionKey(): ByteArray = publicEncryptionKey

    override fun behaviorBitField(): Int = behaviorBitField

    open class Factory {

        open fun create(input: InputStream, streamNumber: Long): PublicKeyV2 = PublicKeyV2(
                version = 2L,
                streamNumber = streamNumber,
                behaviorBitField = decoding.uint32(input).toInt(),
                publicSigningKey = decoding.bytes(input, 64),
                publicEncryptionKey = decoding.bytes(input, 64)
        )

        open fun create(version: Long, streamNumber: Long, behaviorBitField: Int,
                publicSigningKey: ByteArray, publicEncryptionKey: ByteArray
        ): PublicKeyV2 = PublicKeyV2(
                version = version,
                streamNumber = streamNumber,
                behaviorBitField = behaviorBitField,
                publicSigningKey = publicSigningKey,
                publicEncryptionKey = publicEncryptionKey
        )
    }
}