package entities.messages.base

/**
 * Created by i_komarov on 05.07.17.
 */
enum class ObjectType(val number: Long) {
    PUBLIC_KEY_GET (0L),
    PUBLIC_KEY     (1L),
    MESSAGE        (2L),
    BROADCAST      (3L),
    NULL           (-1L);
}