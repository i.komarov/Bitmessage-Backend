package entities.messages.base

import base.Streamable

/**
 * Created by i_komarov on 04.07.17.
 */

interface IMessagePayload: Streamable {

    fun command(): Command
}