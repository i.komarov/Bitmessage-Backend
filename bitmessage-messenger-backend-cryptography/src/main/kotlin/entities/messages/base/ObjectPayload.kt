package entities.messages.base

import base.Streamable
import java.io.OutputStream

/**
 * Created by i_komarov on 05.07.17.
 */

abstract class ObjectPayload(val version: Long): Streamable {

    abstract fun type(): ObjectType

    abstract fun streamNumber(): Long

    abstract fun version(): Long

    open fun isSigned(): Boolean = false

    open fun sign(output: OutputStream) {

    }

    /**
     * @return the ECDSA signature which, as of protocol v3, covers the object header starting with the time,
     * appended with the data described in this table down to the extra_bytes. Therefore, this must
     * be checked and set in the {@link ObjectCommand} object.
     */
    open fun signature(): ByteArray? = null

    /**
     * @param signature the ECDSA signature which, as of protocol v3, covers the object header starting with the time,
     * appended with the data described in this table down to the extra_bytes. Therefore, this must
     * be checked and set in the {@link ObjectCommand} object.
     */
    open fun signature(signature: ByteArray?) {

    }
}