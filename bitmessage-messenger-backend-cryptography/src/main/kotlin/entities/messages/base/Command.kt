package entities.messages.base

/**
 * Created by i_komarov on 04.07.17.
 */
enum class Command(val command: String) {
    VERSION   ("VERSION"),
    VERACK    ("VERACK") ,
    ADDRESS   ("ADDR")   ,
    INVENTORY ("INV")    ,
    GETDATA   ("GETDATA"),
    OBJECT    ("OBJECT") ,
    CUSTOM    ("CUSTOM") ,
}