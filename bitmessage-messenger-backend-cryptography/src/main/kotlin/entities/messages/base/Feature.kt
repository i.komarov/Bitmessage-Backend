package entities.messages.base

/**
 * Created by i_komarov on 05.07.17.
 */

enum class Feature(bitNumber: Int) {
    /**
     * Receiving node expects that the RIPE hash encoded in their entities.address proceeds the encrypted entities.messages data of msg
     * messages bound for them.
     */
    INCLUDE_DESTINATION(30),
    /**
     * When true, the receiving node does send acknowledgements (rather than dropping them).
     */
    PERFORMS_ACKNOWLEDGMENT(31);

    val bitNumber: Int

    init {
        this.bitNumber = bitNumber.shl(31 - bitNumber)
    }

    companion object {
        fun bitField(vararg features: Feature): Int {
            var bits: Int = 0
            features.forEach { bits = bits and it.bitNumber }
            return bits
        }

        fun features(bitfield: Int): Array<Feature> = Feature.values().filter {
            bitfield and it.bitNumber != 0
        }.toTypedArray()
    }

    fun isActive(bitField: Int): Boolean = (bitField and bitNumber) != 0
}