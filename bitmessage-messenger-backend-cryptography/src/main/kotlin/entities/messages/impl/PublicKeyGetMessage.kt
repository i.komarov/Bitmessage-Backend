package entities.messages.impl

import entities.address.BitmessageAddress
import entities.messages.base.IMessagePayload
import entities.messages.base.ObjectPayload
import entities.messages.base.ObjectType
import java.io.InputStream
import java.io.OutputStream
import java.nio.ByteBuffer

/**
 * Created by i_komarov on 13.07.17.
 */

class PublicKeyGetMessage(
        version: Long,
        val streamNumber: Long,
        val ripeOrTag: ByteArray
): ObjectPayload(version) {

    override fun write(output: OutputStream) {
        output.write(ripeOrTag)
    }

    override fun write(output: ByteBuffer) {
        output.put(ripeOrTag)
    }

    override fun type(): ObjectType = ObjectType.PUBLIC_KEY_GET

    override fun streamNumber(): Long = streamNumber

    override fun version(): Long = version

    class Factory {

        fun create(address: BitmessageAddress): PublicKeyGetMessage = PublicKeyGetMessage(
                version = address.version,
                streamNumber = address.streamNumber,
                ripeOrTag = when {
                    address.version < 4 -> address.ripe
                    else -> address.tag ?: throw IllegalArgumentException("Bitmessage address must contain non null tag since version 4")
                }
        )

        fun create(input: InputStream, version: Long, streamNumber: Long, length: Int): PublicKeyGetMessage = PublicKeyGetMessage(
                version = version,
                streamNumber = streamNumber,
                ripeOrTag = decoding.bytes(input, length)
        )
    }
}