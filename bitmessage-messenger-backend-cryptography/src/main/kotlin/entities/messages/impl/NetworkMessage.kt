package entities.messages.impl

import cores.base.ICryptographyCore
import entities.messages.base.IMessagePayload
import java.io.OutputStream
import java.nio.ByteBuffer

/**
 * Created by i_komarov on 12.07.17.
 */
class NetworkMessage(
        val payload: IMessagePayload
) {

    companion object {
        val serialVersionUID = 702708857104464809L
        //stub used for null-padding command
        val commandStub = ByteArray(12)
        val magicNumber = 0xE9BEB4D9.toInt()
    }

    class Manager(private val core: ICryptographyCore) {

        fun write(message: NetworkMessage, output: OutputStream) = with(message) {
            val payloadBytes = writeHeader(message, output)
            output.write(payloadBytes)
        }

        fun write(message: NetworkMessage, output: ByteBuffer) {
            val payloadBytes = writeHeader(message, output)
            output.put(payloadBytes)
        }

        private fun computeChecksum(bytes: ByteArray): ByteArray {
            val hash = core.sha512(bytes)
            return byteArrayOf(hash[0], hash[1], hash[2], hash[3])
        }

        /**
         * @return messsage payload
         * */
        private fun writeHeader(message: NetworkMessage, output: OutputStream): ByteArray = with(message) {
            //put magic number
            encoding.varInt(magicNumber.toLong(), output)
            //put null-padded command
            output.write(nullPaddCommand(message.payload.command().command))
            //payload size
            val payloadBytes = encoding.bytes(payload)
            encoding.int32(payloadBytes.size.toLong(), output)
            //checksum
            output.write(computeChecksum(payloadBytes))
            //return payload bytes
            return payloadBytes
        }

        /**
         * @return messsage payload
         * */
        private fun writeHeader(message: NetworkMessage, output: ByteBuffer): ByteArray = with(message) {
            //put magic number
            encoding.varInt(magicNumber.toLong(), output)
            //put command
            val command = payload.command().command.toLowerCase()
            output.put(command.toByteArray(charset("US-ASCII")))
            //null padding
            for(it in command.length..11) {
                output.put(0.toByte())
            }
            //payload size
            val payloadBytes = encoding.bytes(payload)
            encoding.int32(payloadBytes.size.toLong(), output)
            //checksum
            output.put(computeChecksum(payloadBytes))
            //return payload bytes
            return payloadBytes
        }

        private fun nullPaddCommand(command: String): ByteArray {
            val bytes = command.toLowerCase().toByteArray(charset("US-ASCII"))
            val stub = commandStub.copyOf()
            System.arraycopy(bytes, 0, stub, 0, bytes.size)
            return bytes
        }
    }
}