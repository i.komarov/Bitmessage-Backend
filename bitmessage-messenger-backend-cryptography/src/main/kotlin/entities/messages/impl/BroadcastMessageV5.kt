package entities.messages.impl

import entities.address.BitmessageAddress
import entities.extra.CryptographyBox
import entities.messages.plain_text.PlaintextMessage
import java.io.InputStream
import java.io.OutputStream
import java.nio.ByteBuffer

/**
 * Created by i_komarov on 13.07.17.
 */
class BroadcastMessageV5 protected constructor(
        version: Long,
        streamNumber: Long,
        encrypted: CryptographyBox? = null,
        plaintextMessage: PlaintextMessage? = null,
        val tag: ByteArray
): BroadcastMessageV4(version, streamNumber, encrypted, plaintextMessage) {

    companion object {
        val serialVersionUID = 920649721626968644L
    }

    constructor(senderAddress: BitmessageAddress, plaintextMessage: PlaintextMessage): this(
            version = 5L,
            streamNumber = senderAddress.streamNumber,
            plaintextMessage = plaintextMessage,
            tag = senderAddress.tag ?: throw IllegalStateException("Bitmessage address must contain tag sice version 4")
    )

    override fun sign(output: OutputStream) {
        output.write(tag)
        super.sign(output)
    }

    override fun write(output: OutputStream) {
        output.write(tag)
        super.write(output)
    }

    override fun write(output: ByteBuffer) {
        output.put(tag)
        super.write(output)
    }

    class Factory(private val boxFactory: CryptographyBox.Factory) {

        fun create(input: InputStream, streamNumber: Long, length: Int): BroadcastMessageV5 = BroadcastMessageV5(
                version = 5L,
                streamNumber = streamNumber,
                tag = decoding.bytes(input, 32),
                encrypted = boxFactory.create(input, length - 32)
        )
    }
}