package entities.messages.impl

import entities.address.BitmessageAddress
import entities.encryption.IDecryptionVisitor
import entities.encryption.IEncryptableElement
import entities.encryption.IEncryptionVisitor
import entities.extra.CryptographyBox
import entities.messages.base.ObjectPayload
import entities.messages.base.ObjectType
import entities.messages.plain_text.PlaintextMessage
import java.util.*

/**
 * Created by i_komarov on 13.07.17.
 */
abstract class BroadcastMessage(
        version: Long,
        val streamNumber: Long,
        var plaintextMessage: PlaintextMessage? = null,
        var encrypted: CryptographyBox? = null
): ObjectPayload(version), IEncryptableElement {

    companion object {

        fun getBroadcastVersionForBitmessageAddress(address: BitmessageAddress): Long = when {
            address.version < 4L -> 4L
            else -> 5L
        }
    }

    override fun type(): ObjectType = ObjectType.BROADCAST

    override fun streamNumber(): Long = streamNumber

    override fun version(): Long = version

    override fun isDecrypted(): Boolean = plaintextMessage != null

    override fun isSigned(): Boolean = true

    override fun signature(): ByteArray? = plaintextMessage?.signature()

    override fun signature(signature: ByteArray?) {
        signature?.let { plaintextMessage?.signature(it) }
    }

    override fun acceptEncryptionVisitor(visitor: IEncryptionVisitor, publicKey: ByteArray) {
        visitor.visit(this, publicKey)
    }

    override fun acceptDecryptionVisitor(visitor: IDecryptionVisitor, privateKey: ByteArray) {
        visitor.visit(this, privateKey)
    }

    override fun equals(other: Any?): Boolean = when {
        this == other -> true
        other == null || this::javaClass != other::javaClass -> false
        else -> {
            other as BroadcastMessage

            streamNumber == other.streamNumber &&
                    (Objects.equals(plaintextMessage, other.plaintextMessage) ||
                     Objects.equals(encrypted, other.encrypted))
        }
    }

    override fun hashCode(): Int = Objects.hash(streamNumber)
}