package entities.messages.impl

import entities.messages.base.ObjectPayload
import entities.messages.base.ObjectType
import java.io.InputStream
import java.io.OutputStream
import java.nio.ByteBuffer
import java.util.*

/**
 * Created by i_komarov on 12.07.17.
 */
class GenericPayload(
        version: Long,
        val streamNumber: Long,
        val data: ByteArray
): ObjectPayload(version) {

    companion object {
        val serialVersionUID = -912314085064185940L
    }

    override fun write(output: OutputStream) {
        output.write(data)
    }

    override fun write(output: ByteBuffer) {
        output.put(data)
    }

    override fun type(): ObjectType = ObjectType.NULL

    override fun streamNumber(): Long = streamNumber

    override fun version(): Long = super.version

    override fun equals(other: Any?): Boolean = when {
        this == other -> true
        other == null || this::javaClass != other.javaClass -> false
        else -> {
            other as GenericPayload
            if(this.streamNumber != other.streamNumber) {
                false
            } else {
                Arrays.equals(this.data, other.data)
            }
        }
    }

    override fun hashCode(): Int {
        val streamHashPart = (streamNumber xor (streamNumber ushr 32)).toInt()
        return 31 * streamHashPart + Arrays.hashCode(data)
    }

    class Factory {

        fun create(input: InputStream, version: Long, streamNumber: Long, length: Int): GenericPayload = GenericPayload(
                version = version,
                streamNumber = streamNumber,
                data = decoding.bytes(input, length)
        )
    }
}