package entities.messages.impl

import cores.base.ICryptographyCore
import engine.base.ICryptographyEngine
import entities.extra.CryptographyBox
import entities.keys.PublicKey
import entities.keys.PublicKeyV2
import entities.keys.PublicKeyV3
import entities.keys.PublicKeyV4
import entities.messages.base.ObjectPayload
import entities.messages.base.ObjectType
import java.io.InputStream

/**
 * Created by i_komarov on 13.07.17.
 */

class MessagesReader(private val core: ICryptographyCore, private val engine: ICryptographyEngine) {

    private val publicKeyGetMessageFactory by lazy { PublicKeyGetMessage.Factory() }

    private val publicKeyFactoryV2 by lazy { PublicKeyV2.Factory() }
    private val publicKeyFactoryV3 by lazy { PublicKeyV3.Factory() }
    private val publicKeyFactoryV4 by lazy { PublicKeyV4.Factory(core, CryptographyBox.Factory(core, engine)) }

    private val broadcastFactoryV4 by lazy { BroadcastMessageV4.Factory(CryptographyBox.Factory(core, engine)) }
    private val broadcastFactoryV5 by lazy { BroadcastMessageV5.Factory(CryptographyBox.Factory(core, engine)) }

    private val messageFactory by lazy { Message.Factory(CryptographyBox.Factory(core, engine)) }

    fun read(input: InputStream, objectType: Long, version: Long, streamNumber: Long, length: Int): ObjectPayload = when(objectType) {
        ObjectType.PUBLIC_KEY.number -> readPublicKeyMessage(
                input = input,
                version = version,
                streamNumber = streamNumber,
                length = length
        )
        ObjectType.PUBLIC_KEY_GET.number -> readPublicKeyGetMessage(
                input = input,
                version = version,
                streamNumber = streamNumber,
                length = length
        )
        ObjectType.BROADCAST.number -> readBroadcastMessage(
                input = input,
                version = version,
                streamNumber = streamNumber,
                length = length
        )
        ObjectType.MESSAGE.number -> readMessage(
                input = input,
                version = version,
                streamNumber = streamNumber,
                length = length
        )
        else -> throw IllegalArgumentException("Unrecognizable object type key: $objectType")
    }

    fun readPublicKeyMessage(input: InputStream, version: Long, streamNumber: Long, length: Int): PublicKey = when(version) {
        2L -> publicKeyFactoryV2.create(
                input = input,
                streamNumber = streamNumber
        )
        3L -> publicKeyFactoryV3.create(
                input = input,
                streamNumber = streamNumber
        )
        4L -> publicKeyFactoryV4.create(
                input = input,
                streamNumber = streamNumber
        )
        else -> throw IllegalArgumentException("Unrecognizable public key version: $version")
    }

    fun readPublicKeyGetMessage(input: InputStream, version: Long, streamNumber: Long, length: Int): PublicKeyGetMessage = publicKeyGetMessageFactory.create(
            input = input,
            version = version,
            streamNumber = streamNumber,
            length = length
    )

    fun readBroadcastMessage(input: InputStream, version: Long, streamNumber: Long, length: Int): BroadcastMessage = when(version) {
        4L -> broadcastFactoryV4.create(
                input = input,
                streamNumber = streamNumber,
                length = length
        )
        5L -> broadcastFactoryV5.create(
                input = input,
                streamNumber = streamNumber,
                length = length
        )
        else -> throw IllegalArgumentException("Unrecognizable broadcast message version: $version")
    }

    fun readMessage(input: InputStream, version: Long, streamNumber: Long, length: Int): Message = messageFactory.create(
            input = input,
            version = version,
            streamNumber = streamNumber,
            length = length
    )
}