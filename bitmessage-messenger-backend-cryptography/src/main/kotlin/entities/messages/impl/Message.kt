package entities.messages.impl

import entities.encryption.IDecryptionVisitor
import entities.encryption.IEncryptableElement
import entities.encryption.IEncryptionVisitor
import entities.extra.CryptographyBox
import entities.messages.base.ObjectPayload
import entities.messages.base.ObjectType
import entities.messages.plain_text.PlaintextMessage
import java.io.InputStream
import java.io.OutputStream
import java.nio.ByteBuffer
import java.util.*

/**
 * Created by i_komarov on 13.07.17.
 */
class Message(
        version: Long,
        val streamNumber: Long,
        var encrypted: CryptographyBox? = null,
        var plaintextMessage: PlaintextMessage? = null
) : ObjectPayload(version), IEncryptableElement {

    companion object {
        val serialVersionUID = 4327495048296365733L
    }

    protected constructor(streamNumber: Long, encrypted: CryptographyBox): this(
            version = 1L,
            streamNumber = streamNumber,
            encrypted = encrypted
    )

    protected constructor(plaintextMessage: PlaintextMessage): this(
            version = 1L,
            streamNumber = plaintextMessage.streamNumber(),
            plaintextMessage = plaintextMessage
    )

    override fun type(): ObjectType = ObjectType.MESSAGE

    override fun streamNumber(): Long = streamNumber

    override fun version(): Long = 1L

    override fun isDecrypted(): Boolean = plaintextMessage != null

    override fun acceptEncryptionVisitor(visitor: IEncryptionVisitor, publicKey: ByteArray) {
        visitor.visit(this, publicKey)
    }

    override fun acceptDecryptionVisitor(visitor: IDecryptionVisitor, privateKey: ByteArray) {
        visitor.visit(this, privateKey)
    }

    override fun sign(output: OutputStream) {
        plaintextMessage?.write(output, false)
    }

    override fun isSigned(): Boolean = true

    override fun signature(): ByteArray? = plaintextMessage?.signature

    override fun signature(signature: ByteArray?) {
        signature?.let { plaintextMessage?.signature(it) }
    }

    override fun write(output: OutputStream) {
        if(encrypted == null) {
            throw IllegalStateException("Message can not be written before encryption and signing")
        }

        encrypted?.write(output)
    }

    override fun write(output: ByteBuffer) {
        if(encrypted == null) {
            throw IllegalStateException("Message can not be written before encryption and signing")
        }

        encrypted?.write(output)
    }

    override fun equals(other: Any?): Boolean = when {
        this == other -> true
        other == null || this::javaClass != other::javaClass -> false
        else -> {
            other as Message

            streamNumber == other.streamNumber &&
                    (Objects.equals(encrypted, other.encrypted) ||
                     Objects.equals(plaintextMessage, other.plaintextMessage))
        }
    }

    override fun hashCode(): Int = streamNumber.toInt()

    class Factory(private val boxFactory: CryptographyBox.Factory) {

        fun create(input: InputStream, version: Long, streamNumber: Long, length: Int): Message = Message(
                version = version,
                streamNumber = streamNumber,
                encrypted = boxFactory.create(input, length)
        )
    }
}