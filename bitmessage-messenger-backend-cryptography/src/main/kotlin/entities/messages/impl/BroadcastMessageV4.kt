package entities.messages.impl

import entities.address.BitmessageAddress
import entities.extra.CryptographyBox
import entities.messages.plain_text.PlaintextMessage
import java.io.InputStream
import java.io.OutputStream
import java.nio.ByteBuffer

/**
 * Created by i_komarov on 13.07.17.
 */
open class BroadcastMessageV4 protected constructor(
        version: Long,
        streamNumber: Long,
        encrypted: CryptographyBox? = null,
        plaintextMessage: PlaintextMessage? = null
): BroadcastMessage(version, streamNumber, plaintextMessage, encrypted) {

    companion object {
        val serialVersionUID = 195663108282762711L
    }

    constructor(senderAddress: BitmessageAddress, plaintextMessage: PlaintextMessage): this(
            version = 4L,
            streamNumber = senderAddress.streamNumber,
            plaintextMessage = plaintextMessage
    )

    override fun sign(output: OutputStream) {
        plaintextMessage?.write(output, false)
    }

    override fun write(output: OutputStream) {
        encrypted?.write(output)
    }

    override fun write(output: ByteBuffer) {
        encrypted?.write(output)
    }

    class Factory(private val boxFactory: CryptographyBox.Factory) {

        fun create(input: InputStream, streamNumber: Long, length: Int): BroadcastMessageV4 = BroadcastMessageV4(
                version = 4L,
                streamNumber = streamNumber,
                encrypted = boxFactory.create(input, length)
        )
    }
}