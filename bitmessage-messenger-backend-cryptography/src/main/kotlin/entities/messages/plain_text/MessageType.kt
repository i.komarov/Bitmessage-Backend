package entities.messages.plain_text

/**
 * Created by i_komarov on 13.07.17.
 */

enum class MessageType {
    BROADCAST ,
    MESSAGE   ,
}