package entities.messages.plain_text

import base.Streamable
import constants.ACKNOWLEDGEMENT_SIZE
import cores.base.ICryptographyCore
import entities.address.BitmessageAddress
import entities.commands.ObjectCommand
import entities.extra.InventoryVector
import entities.labels.Label
import entities.labels.LabelType
import entities.messages.base.Feature
import entities.messages.base.ObjectType
import entities.messages.impl.GenericPayload
import utils.messageTTL
import utils.unixTime
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.io.OutputStream
import java.nio.ByteBuffer
import java.util.*
import java.util.Objects
import kotlin.collections.HashSet


/**
 * Created by i_komarov on 12.07.17.
 */

class PlaintextMessage private constructor(
        val messageType: MessageType,
        val senderAddress: BitmessageAddress,
        val messageEncoding: Long = MessageEncoding.IGNORE.value,
        val message: ByteArray = ByteArray(0),
        val acknowledgementData: ByteArray? = null,
        var acknowledgementMessage: ObjectCommand? = null,
        var id: Any?,
        var inventoryVector: InventoryVector? = null,
        var receiverAddress: BitmessageAddress? = null,
        var signature: ByteArray? = null,
        var messageStatus: MessageStatus? = null,
        var sent: Long? = null,
        val received: Long? = null,
        val labels: MutableList<Label>? = null,
        var initialHash: ByteArray? = ByteArray(0),
        val ttl: Long = 0L,
        val retries: Int = 0,
        var nextTry: Long? = null
): Streamable {

    fun write(output: OutputStream, includeSignature: Boolean) {
        val isMessage = messageType == MessageType.MESSAGE
        val publicKey = senderAddress.publicKey() ?: throw IllegalStateException("Public key in sender bitmessage address of message can not be null")
        encoding.varInt(senderAddress.version, output)
        encoding.varInt(senderAddress.streamNumber, output)
        encoding.int32(publicKey.behaviorBitField().toLong(), output)
        output.write(publicKey.signingKey(), 1, 64)
        output.write(publicKey.encryptionKey(), 1, 64)
        if(senderAddress.version >= 3) {
            encoding.varInt(publicKey.nonceTrialsPerBytes(), output)
            encoding.varInt(publicKey.extraBytes(), output)
        }
        if(isMessage) {
            val receiverRipe = receiverAddress?.ripe ?: throw IllegalStateException("Receiver bitmessage address must contain ripe in message")
            output.write(receiverRipe)
        }
        encoding.varInt(messageEncoding, output)
        encoding.varInt(message.size.toLong(), output)
        output.write(message)
        if(isMessage) {
            val receiverAddress = this.receiverAddress ?: throw IllegalStateException("Receiver address must be specified for message")
            val acknowledgementMessage = acknowledgementMessage()
            if(receiverAddress.hasFeature(Feature.PERFORMS_ACKNOWLEDGMENT) && acknowledgementMessage != null) {
                with(ByteArrayOutputStream()) {
                    acknowledgementMessage.write(this)
                    encoding.varBytes(toByteArray(), output)
                }
            } else {
                encoding.varInt(0L, output)
            }
        }

        if(includeSignature) {
            val signature = this.signature
            if(signature != null) {
                encoding.varInt(signature.size.toLong(), output)
                output.write(signature)
            } else {
                encoding.varInt(0L, output)
            }
        }
    }

    fun write(output: ByteBuffer, includeSignature: Boolean) {
        val isMessage = messageType == MessageType.MESSAGE
        val publicKey = senderAddress.publicKey() ?: throw IllegalStateException("Public key in sender bitmessage address of message can not be null")
        encoding.varInt(senderAddress.version, output)
        encoding.varInt(senderAddress.streamNumber, output)
        encoding.int32(publicKey.behaviorBitField().toLong(), output)
        output.put(publicKey.signingKey(), 1, 64)
        output.put(publicKey.encryptionKey(), 1, 64)
        if(senderAddress.version >= 3) {
            encoding.varInt(publicKey.nonceTrialsPerBytes(), output)
            encoding.varInt(publicKey.extraBytes(), output)
        }
        if(isMessage) {
            val receiverRipe = receiverAddress?.ripe ?: throw IllegalStateException("Receiver bitmessage address must contain ripe in message")
            output.put(receiverRipe)
        }
        encoding.varInt(messageEncoding, output)
        encoding.varInt(message.size.toLong(), output)
        output.put(message)
        if(isMessage) {
            val receiverAddress = this.receiverAddress ?: throw IllegalStateException("Receiver address must be specified for message")
            val acknowledgementMessage = acknowledgementMessage()
            if(receiverAddress.hasFeature(Feature.PERFORMS_ACKNOWLEDGMENT) && acknowledgementMessage != null) {
                with(ByteArrayOutputStream()) {
                    acknowledgementMessage.write(this)
                    encoding.varBytes(toByteArray(), output)
                }
            } else {
                encoding.varInt(0L, output)
            }
        }

        if(includeSignature) {
            val signature = this.signature
            if(signature != null) {
                encoding.varInt(signature.size.toLong(), output)
                output.put(signature)
            } else {
                encoding.varInt(0L, output)
            }
        }
    }

    override fun write(output: OutputStream) {
        write(output, true)
    }

    override fun write(output: ByteBuffer) {
        write(output, true)
    }

    fun inventoryVector(): InventoryVector? = inventoryVector

    fun inventoryVector(inventoryVector: InventoryVector) {
        this.inventoryVector = inventoryVector
    }

    fun messageType(): MessageType = messageType

    fun message(): ByteArray = message

    fun senderAddress(): BitmessageAddress = senderAddress

    fun receiverAddress(): BitmessageAddress? = receiverAddress

    fun receiverAddress(receiverAddress: BitmessageAddress) {
        val address = this.receiverAddress
        this.receiverAddress = when {
            address != null && address.version != 0L -> throw IllegalStateException("Correct address was already set")
            address != null && !Arrays.equals(address.ripe, receiverAddress.ripe) -> throw IllegalStateException("RIPEs are expected to be the same by were different")
            else -> receiverAddress
        }
    }

    fun labels(): List<Label> = labels ?: emptyList()

    fun streamNumber(): Long = senderAddress.streamNumber

    fun signature(): ByteArray? = signature

    fun signature(signature: ByteArray) {
        this.signature = signature
    }

    fun isUnread(): Boolean {
        return labels?.any { it.type == LabelType.UNREAD } ?: false
    }

    fun id(): Any? = id

    fun id(id: Any) {
        if(this.id != null) throw IllegalStateException("Identifier already set")
        this.id = id
    }

    fun sent(): Long? = sent

    fun received(): Long? = received

    fun status(): MessageStatus? = messageStatus

    fun status(status: MessageStatus) {
        if(status != MessageStatus.RECEIVED && status != MessageStatus.DRAFT && sent == null) {
            sent = unixTime()
        }

        this.messageStatus = status
    }

    fun ttl(): Long = ttl

    fun retries(): Int = retries

    fun nextTry(): Long? = nextTry

    fun updateNextTry() {
        val receiver = receiverAddress
        val next = nextTry
        receiver?.let {
            when(next) {
                null -> {
                    if(sent != null && receiver.hasFeature(Feature.PERFORMS_ACKNOWLEDGMENT)) {
                        nextTry = unixTime(+ttl)
                    }
                }
                else -> {
                    nextTry = next + (1.shl(retries)) * ttl
                }
            }
        }
    }

    fun subject(): String {
        val scanner = Scanner(ByteArrayInputStream(message), "UTF-8")
        val firstLine = scanner.nextLine()
        return when {
            messageEncoding == 2L -> firstLine.substring("Subject:".length).trim()
            firstLine.length > 50 -> firstLine.substring(0, 50).trim() + "..."
            else -> firstLine
        }
    }

    fun text(): String {
        val text = String(message, charset("utf-8"))
        return when(messageEncoding) {
            2L -> text.substring(text.indexOf("\nBody:") + 6)
            else -> text
        }
    }

    fun addLabels(vararg labels: Label) {
        val thisLabels = this.labels
        thisLabels?.addAll(labels)
    }

    fun addLabels(labels: Collection<Label>) {
        val thisLabels = this.labels
        thisLabels?.addAll(labels)
    }

    fun removeLabel(labelType: LabelType) {
        val thisLabels = this.labels
        thisLabels?.removeAll { it.type == labelType }
    }

    fun acknowledgementData(): ByteArray? = acknowledgementData

    fun acknowledgementMessage(): ObjectCommand? {
        var acknowledgementMessage = this.acknowledgementMessage
        val acknowledgementData = this.acknowledgementData
        if(acknowledgementMessage == null && acknowledgementData != null) {
            acknowledgementMessage = ObjectCommand.Builder { withExpirationTime { unixTime(ttl) }
                withObjectType { ObjectType.MESSAGE }
                withPayload { GenericPayload(3, senderAddress.streamNumber, acknowledgementData) }
            }.create()

            this.acknowledgementMessage = acknowledgementMessage
        }

        return acknowledgementMessage
    }

    fun initialHash(): ByteArray? = initialHash

    fun initialHash(initialHash: ByteArray) {
        this.initialHash = initialHash
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        other as PlaintextMessage

        val thisReceiverRipe = receiverAddress?.ripe
        val otherReceiverRipe = other.receiverAddress?.ripe

        return messageEncoding == other.messageEncoding &&
                senderAddress == other.senderAddress &&
                Arrays.equals(message, other.message) &&
                acknowledgementMessage == other.acknowledgementMessage &&
                Arrays.equals(thisReceiverRipe, otherReceiverRipe) &&
                Arrays.equals(signature, other.signature) &&
                messageStatus == other.messageStatus &&
                sent == other.sent &&
                received == other.received &&
                labels == other.labels
    }

    override fun hashCode(): Int {
        return Objects.hash(senderAddress, messageEncoding, message, acknowledgementData, receiverAddress, signature, messageStatus, sent, received, labels)
    }

    class Builder private constructor(val messageType: MessageType) {
        var id: Any? = null
            private set
        var inventoryVector: InventoryVector? = null
            private set
        var senderAddress: BitmessageAddress? = null
            private set
        var receiverAddress: BitmessageAddress? = null
            private set
        var addressVersion: Long = 0
            private set
        var streamNumber: Long = 0
            private set
        var behaviorBitField: Int = 0
            private set
        var publicSigningKey: ByteArray? = null
            private set
        var publicEncryptionKey: ByteArray? = null
            private set
        var nonceTrialsPerByte: Long = 0
            private set
        var extraBytes: Long = 0
            private set
        var destinationRipe: ByteArray? = null
            private set
        var messageEncoding: Long = MessageEncoding.IGNORE.value
            private set
        var message = ByteArray(0)
            private set
        var acknowledgementData: ByteArray? = null
            private set
        var acknowledgementMessage: ByteArray? = null
            private set
        var signature: ByteArray? = null
            private set
        var sent: Long = 0
            private set
        var received: Long = 0
            private set
        var messageStatus: MessageStatus? = null
            private set
        private val labels = HashSet<Label>()
            fun labels(): List<Label> = labels.toList()
        var ttl: Long = 0L
            private set
        var retries: Int = 0
            private set
        var nextTry: Long? = null
            private set

        constructor(messageType: MessageType, init: Builder.() -> Unit): this(messageType) {
            init()
        }

        fun withId(init: Builder.() -> Any): Builder = apply { this.id = init() }

        fun withInventoryVector(init: Builder.() -> InventoryVector): Builder = apply { this.inventoryVector = init() }

        fun withSenderAddress(init: Builder.() -> BitmessageAddress): Builder = apply { this.senderAddress = init() }

        fun withReceiverAddress(init: Builder.() -> BitmessageAddress): Builder = apply { this.receiverAddress = init() }

        fun withAddressVersion(init: Builder.() -> Long): Builder = apply { this.addressVersion = init() }

        fun withStreamNumber(init: Builder.() -> Long): Builder = apply { this.streamNumber = init() }

        fun withBehaviorBitField(init: Builder.() -> Int): Builder = apply { this.behaviorBitField = init() }

        fun withPublicSigningKey(init: Builder.() -> ByteArray): Builder = apply { this.publicSigningKey = init() }

        fun withPublicEncryptionKey(init: Builder.() -> ByteArray): Builder = apply { this.publicEncryptionKey = init() }

        fun withNonceTrialsPerByte(init: Builder.() -> Long): Builder = apply { this.nonceTrialsPerByte = init() }

        fun withExtraBytes(init: Builder.() -> Long): Builder = apply { this.extraBytes = init() }

        fun withDestinationRipe(init: Builder.() -> ByteArray?): Builder = apply { this.destinationRipe = init() }

        fun withMessageEncoding(init: Builder.() -> MessageEncoding): Builder = apply { this.messageEncoding = init().value }

        fun withMessageEncodingValue(init: Builder.() -> Long): Builder = apply { this.messageEncoding = init() }

        fun withMessage(init: Builder.() -> Pair<String, String>): Builder = apply {
            with(init()) {
                this@Builder.messageEncoding = MessageEncoding.SIMPLE.value
                this@Builder.message = ("Subject:" + second + '\n' + "Body:" + first).toByteArray(charset("utf-8"))
            }
        }

        fun withMessageBytes(init: Builder.() -> ByteArray): Builder = apply { this.message = init() }

        fun withAcknowledgementMessage(init: Builder.() -> ByteArray?): Builder = apply {
            with(init()) {
                when {
                    messageType != MessageType.MESSAGE && this != null -> throw IllegalStateException("Only message can contain an acknowledgment")
                    else -> this@Builder.acknowledgementMessage = acknowledgementMessage
                }
            }
        }

        fun withAcknowledgementData(init: Builder.() -> ByteArray?): Builder = apply {
            with(init()) {
                when {
                    messageType != MessageType.MESSAGE && this != null -> throw IllegalStateException("Only message can contain an acknowledgment")
                    else -> this@Builder.acknowledgementData = acknowledgementData
                }
            }
        }

        fun withSignature(init: Builder.() -> ByteArray): Builder = apply { this.signature = init() }

        fun withSent(init: Builder.() -> Long): Builder = apply { this.sent = init() }

        fun withReceived(init: Builder.() -> Long): Builder = apply { this.received = init() }

        fun withStatus(init: Builder.() -> MessageStatus): Builder = apply { this.messageStatus = init() }

        fun withLabels(init: Builder.() -> Collection<Label>): Builder = apply { this.labels.addAll(init()) }

        fun withTTL(init: Builder.() -> Long): Builder = apply { this.ttl = init() }

        fun withRetries(init: Builder.() -> Int): Builder = apply { retries = init() }

        fun withNextTry(init: Builder.() -> Long): Builder = apply { nextTry = init() }
    }

    class Factory(private val core: ICryptographyCore, private val addressFactory: BitmessageAddress.Factory, private val publicKeyFactory: entities.keys.Factory, private val objectCommandFactory: ObjectCommand.Factory) {

        fun create(input: InputStream, messageType: MessageType): PlaintextMessage = createWithoutSignature(input = input, messageType = messageType) {
            withSignature { decoding.varBytes(input) }
            withReceived { unixTime() }
        }.let { create(it) }

        fun createWithoutSignature(input: InputStream, messageType: MessageType, postProcessor: (Builder.() -> Unit)?):
                PlaintextMessage.Builder {
            val version = decoding.varInt(input)
            return Builder(messageType = messageType, init = {
                withAddressVersion { version }
                withStreamNumber { decoding.varInt(input) }
                withBehaviorBitField { decoding.int32(input) }
                withPublicSigningKey { decoding.bytes(input, 64) }
                withPublicEncryptionKey { decoding.bytes(input, 64) }
                withNonceTrialsPerByte { when {
                    version >= 3 -> decoding.varInt(input)
                    else -> 0
                } }
                withExtraBytes { when {
                    version >= 3 -> decoding.varInt(input)
                    else -> 0
                } }
                withDestinationRipe { when {
                    messageType == MessageType.MESSAGE -> decoding.bytes(input, 20)
                    else -> null
                } }
                withMessageEncodingValue { decoding.varInt(input) }
                withMessageBytes { decoding.varBytes(input) }
                withAcknowledgementMessage { when {
                    messageType == MessageType.MESSAGE -> decoding.varBytes(input)
                    else -> null
                } }
                postProcessor?.invoke(this)
            })
        }

        fun create(builder: Builder): PlaintextMessage {
            val tempSenderAddress = builder.senderAddress
            val tempReceiverAddress = builder.receiverAddress
            val tempDestinationRipe = builder.destinationRipe
            val tempAcknowledgementData = builder.acknowledgementData
            val tempAcknowledgementMessage = builder.acknowledgementMessage

            val senderAddress = tempSenderAddress ?: addressFactory.create(
                    publicKeyFactory.generatePublicKey(
                            version = builder.addressVersion,
                            streamNumber = builder.streamNumber,
                            publicSigningKey = builder.publicSigningKey ?: throw IllegalStateException("Either sender address or public signing key and public encryption key must be specified"),
                            publicEncryptionKey = builder.publicEncryptionKey ?: throw IllegalStateException("Either sender address or public signing key and public encryption key must be specified"),
                            nonceTrialsPerByte = builder.nonceTrialsPerByte,
                            extraBytes = builder.extraBytes,
                            behaviorBitField = builder.behaviorBitField
                    )
            )

            val receiverAddress = when {
                tempReceiverAddress == null && builder.messageType != MessageType.BROADCAST && tempDestinationRipe != null -> {
                    addressFactory.create(
                            version = 0L,
                            streamNumber = 0L,
                            ripe = tempDestinationRipe
                    )
                }
                else -> tempReceiverAddress
            }

            val acknowledgementData = when {
                builder.messageType == MessageType.MESSAGE && builder.acknowledgementMessage == null && builder.acknowledgementData == null -> {
                    core.randomBytes(ACKNOWLEDGEMENT_SIZE)
                }
                else -> tempAcknowledgementData
            }

            val acknowledgementMessage = when {
                tempAcknowledgementMessage != null && tempAcknowledgementMessage.isNotEmpty() -> {
                    objectCommandFactory.create(
                            input = ByteArrayInputStream(tempAcknowledgementMessage),
                            length = tempAcknowledgementMessage.size
                    )
                }
                else -> null
            }

            val ttl = when {
                builder.ttl <= 0 -> messageTTL()
                else -> builder.ttl
            }

            return PlaintextMessage(
                    id = builder.id,
                    inventoryVector = builder.inventoryVector,
                    messageType = builder.messageType,
                    senderAddress = senderAddress,
                    receiverAddress = receiverAddress,
                    messageEncoding = builder.messageEncoding,
                    message = builder.message,
                    acknowledgementData = acknowledgementData,
                    acknowledgementMessage = acknowledgementMessage,
                    signature = builder.signature,
                    messageStatus = builder.messageStatus,
                    sent = builder.sent,
                    received = builder.received,
                    labels = builder.labels().toMutableList(),
                    ttl = ttl,
                    retries = builder.retries,
                    nextTry = builder.nextTry
            )
        }
    }
}