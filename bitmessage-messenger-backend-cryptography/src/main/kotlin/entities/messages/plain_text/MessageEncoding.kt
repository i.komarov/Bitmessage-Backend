package entities.messages.plain_text

/**
 * Created by i_komarov on 13.07.17.
 */

enum class MessageEncoding(val value: Long) {
    IGNORE  (0),
    TRIVIAL (1),
    SIMPLE  (2),
}