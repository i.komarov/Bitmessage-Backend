package entities.messages.plain_text

/**
 * Created by i_komarov on 13.07.17.
 */

enum class MessageStatus {
    DRAFT                ,
    PUBLIC_KEY_REQUESTED ,
    PROOF_OF_WORK        ,
    SENT                 ,
    ACKNOWLEDGED         ,
    RECEIVED             ,
}