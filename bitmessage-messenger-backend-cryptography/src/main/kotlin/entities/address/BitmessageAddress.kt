package entities.address

import cores.base.ICryptographyCore
import decoding.base58_decode
import encoding.base58_encode
import engine.base.ICryptographyEngine
import entities.keys.PrivateKey
import entities.keys.PublicKey
import entities.messages.base.Feature
import statistics.AccessCounter
import utils.expand
import utils.leadingZeros
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.Serializable
import java.util.*
import kotlin.properties.Delegates

/**
 * Created by i_komarov on 11.07.17.
 */
class BitmessageAddress(
        val version: Long,
        val streamNumber: Long,
        val ripe: ByteArray,
        val tag: ByteArray? = null,
        val publicDecryptionKey: ByteArray
): Serializable {

    companion object {
        val serialVersionUID: Long = 2386328540805994064L
    }

    private var address: String by Delegates.notNull()
    private var privateKey: PrivateKey by Delegates.notNull()
    private var publicKey: PublicKey? = null
    private var alias: String by Delegates.notNull()
    private var isSubscribed: Boolean = false
    private var isChannel: Boolean = false

    fun address(): String = address

    fun privateKey(): PrivateKey = privateKey

    fun publicKey(): PublicKey? = publicKey

    fun publicKey(publicKey: PublicKey) {
        this.publicKey = publicKey
    }

    fun alias(): String = alias

    fun alias(alias: String) {
        this.alias = alias
    }

    fun isSubscribed(): Boolean = isSubscribed

    fun isSubscribed(isSubscribed: Boolean) {
        this.isSubscribed = isSubscribed
    }

    fun isChannel(): Boolean = isChannel

    fun isChannel(isChannel: Boolean) {
        this.isChannel = isChannel
    }

    fun hasFeature(feature: Feature?): Boolean {
        val publicKey = this.publicKey
        return when {
            publicKey == null || feature == null -> false
            else -> feature.isActive(publicKey.behaviorBitField())
        }
    }

    override fun equals(other: Any?): Boolean = when {
        this == other -> true
        other == null -> false
        else -> {
            other as BitmessageAddress

            Objects.equals(version, other.version) &&
            Objects.equals(streamNumber, other.streamNumber) &&
            Arrays.equals(ripe, other.ripe)
        }
    }

    override fun hashCode(): Int = Arrays.hashCode(ripe)

    class Factory(protected val core: ICryptographyCore, protected val engine: ICryptographyEngine, protected val keyFactory: PrivateKey.Factory) {

        fun deterministic(passphrase: String, numberOfAddresses: Int, version: Long, streamNumber: Long, shorter: Boolean): List<BitmessageAddress> {
            val privateKeys = keyFactory.determenistic(passphrase, numberOfAddresses, version, streamNumber, shorter)
            return List(numberOfAddresses, { index ->
                create(privateKeys[index])
            })
        }

        fun createChannel(address: String, passphrase: String): BitmessageAddress = create(address, passphrase)
                .apply { isChannel = true }

        fun createChannel(streamNumber: Long, passphrase: String): BitmessageAddress = create(
                keyFactory.create(
                        version = PublicKey.latestVersion,
                        streamNumber = streamNumber,
                        passphrase = passphrase
                )
        ).apply { isChannel = true }

        fun create(address: String, passphrase: String): BitmessageAddress = create(address)
                .apply {
                    this.privateKey = keyFactory.create(passphrase, version, streamNumber)
                    this.publicKey = privateKey.publicKey
                    if(!Arrays.equals(ripe, core.generateRipe(privateKey.publicKey))) {
                        throw IllegalArgumentException("Wrong address or passphrase")
                    }
                }

        fun create(privateKey: PrivateKey): BitmessageAddress = create(privateKey.publicKey)
                .apply {
                    this.privateKey = privateKey
                }

        fun create(publicKey: PublicKey): BitmessageAddress = create(
                publicKey.version,
                publicKey.streamNumber(),
                core.generateRipe(publicKey)
        ).apply {
            this.publicKey = publicKey
        }

        fun create(version: Long, streamNumber: Long, ripe: ByteArray): BitmessageAddress {
            var address: String
            var tag: ByteArray?
            var publicDecryptionKey: ByteArray
            with(ByteArrayOutputStream()) {
                encoding.varInt(version, this)
                encoding.varInt(streamNumber, this)
                when {
                    version < 4 -> {
                        val checksum = core.sha512(toByteArray(), ripe)
                        tag = null
                        publicDecryptionKey = Arrays.copyOfRange(checksum, 0, 32)
                    }
                    else -> {
                        // for tag and decryption key, the checksum has to be created with 0x00 padding
                        val checksum = core.doubleSha512(toByteArray(), ripe)
                        tag = Arrays.copyOfRange(checksum, 32, 64)
                        publicDecryptionKey = Arrays.copyOfRange(checksum, 0, 32)
                    }
                }
                // but for the address and its checksum they need to be stripped
                val offset = leadingZeros(ripe)
                write(ripe, offset, ripe.size - offset)
                val checksum = core.doubleSha512(toByteArray())
                write(checksum, 0, 4)
                address = "BM-" + base58_encode(toByteArray())

                return BitmessageAddress(
                        version = version,
                        streamNumber = streamNumber,
                        ripe = ripe,
                        tag = tag,
                        publicDecryptionKey = publicDecryptionKey
                ).apply {
                    this.address = address
                }
            }
        }

        fun create(address: String): BitmessageAddress {
            var tag: ByteArray?
            var publicDecryptionKey: ByteArray
            val bytes: ByteArray = base58_decode(address.substring(3))
            val counter: AccessCounter = AccessCounter()
            with(ByteArrayInputStream(bytes)) {
                val version = decoding.varInt(this, counter)
                val streamNumber = decoding.varInt(this, counter)
                val ripe = expand(decoding.bytes(this, bytes.size - counter.length - 4), 20)
                var checksum: ByteArray = core.doubleSha512(bytes, bytes.size - 4)
                val expectedChecksum: ByteArray = decoding.bytes(this, 4)
                if(!Arrays.equals(checksum, expectedChecksum)) throw IllegalArgumentException("AddressCommand checksum verification failed")
                when {
                    version < 4 -> {
                        checksum = core.sha512(Arrays.copyOfRange(bytes, 0, counter.length), ripe)
                        tag = null
                        publicDecryptionKey = Arrays.copyOfRange(checksum, 0, 32)
                    }
                    else -> {
                        checksum = core.doubleSha512(Arrays.copyOfRange(bytes, 0, counter.length), ripe)
                        tag = Arrays.copyOfRange(checksum, 32, 64)
                        publicDecryptionKey = Arrays.copyOfRange(checksum, 0, 32)
                    }
                }

                return BitmessageAddress(
                        version = version,
                        streamNumber = streamNumber,
                        ripe = ripe,
                        tag = tag,
                        publicDecryptionKey = publicDecryptionKey
                ).apply {
                    this.address = address
                }
            }
        }
    }
}