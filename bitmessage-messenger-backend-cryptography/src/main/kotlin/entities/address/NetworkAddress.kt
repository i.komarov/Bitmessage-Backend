package entities.address

import base.Streamable
import utils.unixTime
import java.io.OutputStream
import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.SocketAddress
import java.nio.ByteBuffer
import java.util.*
import kotlin.properties.Delegates

/**
 * Created by i_komarov on 04.07.17.
 */

class NetworkAddress(var time: Long, val streamNumber: Long, val services: Long, val ipv6: ByteArray?, val port: Int): Streamable {

    companion object {
        val serialVersionUID: Long = 2500120578167100300L
    }

    fun toInetAddress(): InetAddress = InetAddress.getByAddress(ipv6)

    override fun write(output: OutputStream) {
        write(output, false)
    }

    fun write(output: OutputStream, light: Boolean): Unit = with(output) {
        if(!light) {
            encoding.int64(time, this)
            encoding.int32(streamNumber, this)
        }
        encoding.int64(services, this)
        write(ipv6)
        encoding.int16(port.toLong(), this)
    }

    override fun write(output: ByteBuffer) {
        write(output, false)
    }

    fun write(output: ByteBuffer, light: Boolean): Unit = with(output) {
        if(!light) {
            encoding.int64(time, this)
            encoding.int32(streamNumber, this)
        }
        encoding.int64(services, this)
        put(ipv6)
        encoding.int16(port.toLong(), this)
    }

    override fun equals(other: Any?): Boolean = when {
        this == other -> true
        other == null -> false
        else -> {
            other as NetworkAddress
            port == other.port &&
            Arrays.equals(ipv6, other.ipv6)
        }
    }

    override fun hashCode(): Int = when {
        ipv6 == null -> 0
        else -> Arrays.hashCode(ipv6)
    } * 31 + port

    override fun toString(): String = "[${toInetAddress()}]:$port"

    class Builder {

        private var time: Long? = null
        private var stream: Long by Delegates.notNull<Long>()
        private var services: Long by Delegates.notNull<Long>()
        private var ipv6: ByteArray by Delegates.notNull<ByteArray>()
        private var port: Int by Delegates.notNull<Int>()

        fun withTime(time: Long): Builder = apply {
            this.time = time
        }

        fun withStream(stream: Long): Builder = apply {
            this.stream = stream
        }

        fun withServices(services: Long): Builder = apply {
            this.services = services
        }

        fun withIP(ip: InetAddress): Builder = apply {
            when(ip.address.size) {
                16 -> this.ipv6 = ip.address
                4 -> {
                    this.ipv6 = ByteArray(16)
                    this.ipv6[10] = 0xFF.toByte()
                    this.ipv6[11] = 0xFF.toByte()
                    System.arraycopy(ip.address, 0, this.ipv6, 12, 4)
                }
                else -> throw IllegalStateException("Weird ip entities.address.NetworkAddress: ${ip}")
            }
        }

        fun withIPv6(ipv6: ByteArray): Builder = apply {
            this.ipv6 = ipv6
        }

        fun withIPv6(p00: Int, p01: Int, p02: Int, p03: Int,
                     p04: Int, p05: Int, p06: Int, p07: Int,
                     p08: Int, p09: Int, p10: Int, p11: Int,
                     p12: Int, p13: Int, p14: Int, p15: Int): Builder = apply {
            this.ipv6 = ByteArray(16).apply {
                this[0] = p00.toByte()
                this[1] = p01.toByte()
                this[2] = p02.toByte()
                this[3] = p03.toByte()
                this[4] = p04.toByte()
                this[5] = p05.toByte()
                this[6] = p06.toByte()
                this[7] = p07.toByte()
                this[8] = p08.toByte()
                this[9] = p09.toByte()
                this[10] = p10.toByte()
                this[11] = p11.toByte()
                this[12] = p12.toByte()
                this[13] = p13.toByte()
                this[14] = p14.toByte()
                this[15] = p15.toByte()
            }
        }

        fun withPort(port: Int): Builder = apply {
            this.port = port
        }

        fun withAddress(address: SocketAddress): Builder = apply {
            when {
                address is InetSocketAddress -> {
                    withIP(address.address)
                    withPort(address.port)
                }
                else -> throw IllegalStateException("Unknown type of entities.address: ${address::class.java}")
            }
        }

        fun build(): NetworkAddress = NetworkAddress(
                time = time ?: unixTime(),
                streamNumber = stream,
                services = services,
                ipv6 = ipv6,
                port = port
        )
    }
}