package entities.commands

import constants.CURRENT_VERSION
import constants.USER_AGENT
import entities.address.NetworkAddress
import entities.messages.base.IMessagePayload
import entities.messages.base.Command
import utils.unixTime
import java.io.OutputStream
import java.nio.ByteBuffer
import kotlin.properties.Delegates


/**
 * Created by i_komarov on 11.07.17.
 */

/**
 * The 'version' customCommand advertises this node's latest supported protocol version upon initiation.
 */
class VersionCommand(
        val version: Int,
        val services: Long,
        val timestamp: Long,
        val receiverAddress: NetworkAddress,
        val senderAddress: NetworkAddress,
        val nonce: Long,
        val userAgent: String,
        val streams: LongArray
): IMessagePayload {

    companion object {
        val serialVersionUID: Long = 7219240857343176567L
    }

    override fun write(output: OutputStream) = with(output) {
        encoding.int32(version.toLong(), this)
        encoding.int64(services, this)
        encoding.int64(timestamp, this)
        receiverAddress.write(this, true)
        senderAddress.write(this, true)
        encoding.int64(nonce, this)
        encoding.varString(userAgent, this)
        encoding.varIntList(streams, this)
    }

    override fun write(output: ByteBuffer) = with(output) {
        encoding.int32(version.toLong(), this)
        encoding.int64(services, this)
        encoding.int64(timestamp, this)
        receiverAddress.write(this, true)
        senderAddress.write(this, true)
        encoding.int64(nonce, this)
        encoding.varString(userAgent, this)
        encoding.varIntList(streams, this)
    }

    override fun command(): Command = Command.VERSION

    class Builder {

        private var version: Int by Delegates.notNull()
        private var services: Long by Delegates.notNull()
        private var timestamp: Long by Delegates.notNull()
        private var receiverAddress: NetworkAddress by Delegates.notNull()
        private var senderAddress: NetworkAddress by Delegates.notNull()
        private var nonce: Long by Delegates.notNull()
        private var userAgent: String by Delegates.notNull()
        private var streams: LongArray by Delegates.notNull()

        fun withDefaults(clientNonce: Long): Builder = apply {
            this.version = CURRENT_VERSION
            this.services = 1
            this.timestamp = unixTime()
            this.nonce = clientNonce
            this.userAgent = USER_AGENT
            this.streams = longArrayOf(1L)
        }
        /**
         * @param version Identifies protocol version being used by the node. Should equal 3. Nodes should disconnect if the remote node's
         * version is lower but continue with the connection if it is higher.
         */
        fun withVersion(version: Int): Builder = apply {
            this.version = version
        }

        /**
         * @param services bitfield of features to be enabled for this connection
         */
        fun withServices(services: Long): Builder = apply {
            this.services = services
        }

        /**
         * @param timestamp standard UNIX timestamp in seconds
         */
        fun withTimestamp(timestamp: Long): Builder = apply {
            this.timestamp = timestamp
        }

        /**
         * @param receiverAddress The network address of the node receiving this message (not including the time or stream number)
         */
        fun withReceiverAddress(receiverAddress: NetworkAddress): Builder = apply {
            this.receiverAddress = receiverAddress
        }

        /**
         * @param senderAddress The network address of the node emitting this message (not including the time or stream number and the ip itself
         * is ignored by the receiver)
         */
        fun withSenderAddress(senderAddress: NetworkAddress): Builder = apply {
            this.senderAddress = senderAddress
        }

        /**
         * @param nonce Random nonce used to detect connections to self.
         */
        fun withNonce(nonce: Long): Builder = apply {
            this.nonce = nonce
        }

        /**
         * @param userAgent User Agent (0x00 if string is 0 bytes long). Sending nodes must not include a user_agent longer than 5000 bytes.
         */
        fun withUserAgent(userAgent: String): Builder = apply {
            this.userAgent = userAgent
        }

        /**
         * @param streams The stream numbers that the emitting node is interested in. Sending nodes must not include more than 160000
         * stream numbers.
         */
        fun withStreams(vararg streams: Long): Builder = apply {
            this.streams = streams
        }

        fun create(): VersionCommand = VersionCommand(
                version = version,
                services = services,
                timestamp = timestamp,
                receiverAddress = receiverAddress,
                senderAddress = senderAddress,
                nonce = nonce,
                userAgent = userAgent,
                streams = streams
        )
    }
}