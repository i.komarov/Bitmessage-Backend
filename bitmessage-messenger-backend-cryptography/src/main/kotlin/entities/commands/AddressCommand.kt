package entities.commands

import entities.address.NetworkAddress
import entities.messages.base.IMessagePayload
import entities.messages.base.Command
import java.io.OutputStream
import java.nio.ByteBuffer

/**
 * Created by i_komarov on 11.07.17.
 */

/**
 * The 'address' customCommand holds a list of known active Bitmessage nodes.
 */
class AddressCommand(val addresses: List<NetworkAddress>): IMessagePayload {

    companion object {
        val serialVersionUID: Long = -5117688017050138720L
    }

    override fun write(output: OutputStream): Unit = with(output) {
        encoding.varInt(addresses.size.toLong(), this)
        addresses.forEach { it.write(this) }
    }

    override fun write(output: ByteBuffer): Unit = with(output) {
        encoding.varInt(addresses.size.toLong(), output)
        addresses.forEach { it.write(this) }
    }

    override fun command(): Command = Command.ADDRESS

    class Builder {

        private val addresses: MutableList<NetworkAddress> by lazy { mutableListOf<NetworkAddress>() }

        fun withAddress(address: NetworkAddress): Builder = apply {
            this.addresses.add(address)
        }

        fun withAddresses(addresses: Collection<NetworkAddress>): Builder = apply {
            this.addresses.addAll(addresses)
        }

        fun create(): AddressCommand = AddressCommand(addresses)
    }
}