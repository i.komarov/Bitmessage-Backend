package entities.commands

import entities.extra.InventoryVector
import entities.messages.base.IMessagePayload
import entities.messages.base.Command
import java.io.OutputStream
import java.nio.ByteBuffer
import java.util.*

/**
 * Created by i_komarov on 11.07.17.
 */

/**
 * The 'inventory' customCommand holds up to 50000 inventory vectors, i.e. hashes of inventory items.
 */
class InventoryCommand(val inventory: List<InventoryVector>): IMessagePayload {

    companion object {
        val serialVersionUID: Long = 3662992522956947145L
    }

    override fun write(output: OutputStream): Unit = with(output) {
        encoding.varInt(inventory.size.toLong(), this)
        inventory.forEach { it.write(this) }
    }

    override fun write(output: ByteBuffer): Unit = with(output) {
        encoding.varInt(inventory.size.toLong(), this)
        inventory.forEach { it.write(this) }
    }

    override fun command(): Command = Command.INVENTORY

    class Builder {

        private val inventory: LinkedList<InventoryVector> by lazy { LinkedList<InventoryVector>() }

        fun withInventoryVector(inventoryVector: InventoryVector): Builder = apply {
            inventory.add(inventoryVector)
        }

        fun withInventoryVectors(inventoryVectors: Collection<InventoryVector>): Builder = apply {
            inventory.addAll(inventoryVectors)
        }

        fun create(): InventoryCommand = InventoryCommand(inventory)
    }
}