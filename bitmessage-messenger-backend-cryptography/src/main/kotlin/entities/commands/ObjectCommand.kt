package entities.commands

import cores.base.ICryptographyCore
import engine.base.ICryptographyEngine
import entities.encryption.IDecryptionVisitor
import entities.encryption.IEncryptableElement
import entities.encryption.IEncryptionVisitor
import entities.extra.InventoryVector
import entities.keys.PrivateKey
import entities.keys.PublicKey
import entities.messages.base.IMessagePayload
import entities.messages.base.Command
import entities.messages.base.ObjectPayload
import entities.messages.base.ObjectType
import entities.messages.impl.GenericPayload
import entities.messages.impl.MessagesReader
import statistics.AccessCounter
import sun.reflect.generics.reflectiveObjects.NotImplementedException
import utils.truncate
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.io.OutputStream
import java.nio.ByteBuffer
import kotlin.properties.Delegates

/**
 * Created by i_komarov on 11.07.17.
 */

/**
 * The 'object' customCommand sends an object that is shared throughout the network.
 */
class ObjectCommand(
        var nonce: ByteArray?,
        val expirationTime: Long,
        val objectType: Long,
        val version: Long,
        val streamNumber: Long,
        val payload: ObjectPayload,
        var payloadBytes: ByteArray? = null
): IMessagePayload, IEncryptableElement {

    companion object {
        val serialVersionUID: Long = 2495752480120659139L
    }

    override fun write(output: OutputStream) {
        output.write(nonce())
        output.write(payloadBytesWithoutNonce())
    }

    override fun write(output: ByteBuffer) {
        output.put(nonce())
        output.put(payloadBytesWithoutNonce())
    }

    override fun command(): Command = Command.OBJECT

    override fun isDecrypted(): Boolean = payload is IEncryptableElement && payload.isDecrypted()

    override fun acceptEncryptionVisitor(visitor: IEncryptionVisitor, publicKey: ByteArray) {
        visitor.visit(this, publicKey)
    }

    override fun acceptDecryptionVisitor(visitor: IDecryptionVisitor, privateKey: ByteArray) {
        visitor.visit(this, privateKey)
    }

    fun nonce(): ByteArray = nonce ?: ByteArray(8)

    private fun bytesToSign(): ByteArray = with(ByteArrayOutputStream()) {
        writeHeaderWithoutNonce(this)
        payload.sign(this)
        toByteArray()
    }

    private fun writeHeaderWithoutNonce(output: OutputStream) = with(output) {
        encoding.int64(expirationTime, this)
        encoding.int32(objectType, this)
        encoding.varInt(version, this)
        encoding.varInt(streamNumber, this)
    }

    private fun payloadBytesWithoutNonce(): ByteArray {
        if(payloadBytes == null) {
            with(ByteArrayOutputStream()) {
                writeHeaderWithoutNonce(this)
                payload.write(this)
                payloadBytes = toByteArray()
            }
        }

        return payloadBytes ?: ByteArray(0)
    }

    class Builder private constructor() {

        private var nonce: ByteArray by Delegates.notNull()
        private var expirationTime: Long by Delegates.notNull()
        private var objectType: Long = -1L
        private var streamNumber: Long = -1L
        private var payload: ObjectPayload by Delegates.notNull()

        constructor(init: Builder.() -> Unit): this() {
            init()
        }

        fun withNonce(init: Builder.() -> ByteArray): Builder = apply { this.nonce = init() }

        fun withExpirationTime(init: Builder.() -> Long): Builder = apply {
            this.expirationTime = init()
        }

        fun withStreamNumber(init: Builder.() -> Long): Builder = apply {
            this.streamNumber = init()
        }

        fun withObjectType(init: Builder.() -> ObjectType): Builder = apply {
            this.objectType = init().number
        }

        fun withObjectTypeValue(init: Builder.() -> Long): Builder = apply {
            this.objectType = init()
        }

        fun withPayload(init: Builder.() -> ObjectPayload): Builder = apply {
            this.payload = init()
            if(this.objectType == -1L) {
                this.objectType = payload.type().number
            }
        }

        fun create(): ObjectCommand = ObjectCommand(
                nonce = nonce,
                expirationTime = expirationTime,
                objectType = objectType,
                version = payload.version(),
                streamNumber = when {
                    streamNumber > 0 -> streamNumber
                    else -> payload.streamNumber()
                },
                payload = payload
        )
    }

    class Factory(private val payloadReader: MessagesReader) {

        fun create(input: InputStream, length: Int): ObjectCommand {
            val counter = AccessCounter()
            val nonce = decoding.bytes(input, 8, counter)
            val expirationTime = decoding.int64(input, counter)
            val objectType = decoding.uint32(input, counter)
            val version = decoding.varInt(input, counter)
            val streamNumber = decoding.varInt(input, counter)

            val data = decoding.bytes(input, length - counter.length)
            val payload: ObjectPayload = try {
                payloadReader.read(
                        input = ByteArrayInputStream(data),
                        objectType = objectType,
                        version = version,
                        streamNumber = streamNumber,
                        length = length
                )
            } catch(e: Exception) {
                e.printStackTrace()
                GenericPayload(
                        version = version,
                        streamNumber = streamNumber,
                        data = data
                )
            }

            return ObjectCommand.Builder {
                withNonce { nonce }
                withExpirationTime { expirationTime }
                withObjectTypeValue { objectType }
                withStreamNumber { streamNumber }
                withPayload { payload }
            }.create()
        }
    }

    class Manager(private val core: ICryptographyCore, private val engine: ICryptographyEngine) {

        fun isSigned(command: ObjectCommand): Boolean = command.payload.isSigned()

        fun isSignatureValid(command: ObjectCommand, key: PublicKey): Boolean {
            if(!command.isDecrypted()) {
                throw IllegalStateException("Payload must be decrypted before signature verification")
            }
            val signature = command.payload.signature()
            if(signature == null) {
                throw IllegalStateException("Signature does not present")
            }

            return engine.ensureSignatureValidity(command.bytesToSign(), signature, key)
        }

        fun sign(command: ObjectCommand, key: PrivateKey) = with(command) {
            if(payload.isSigned()) {
                payload.signature(engine.generateSignature(bytesToSign(), key))
            }
        }

        fun getInventoryVector(command: ObjectCommand): InventoryVector = InventoryVector(
                truncate(core.doubleSha512(command.nonce(), command.payloadBytesWithoutNonce()), 32)
        )
    }
}