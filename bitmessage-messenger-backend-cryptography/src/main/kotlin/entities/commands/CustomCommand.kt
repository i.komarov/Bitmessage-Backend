package entities.commands

import entities.messages.base.IMessagePayload
import entities.messages.base.Command
import statistics.AccessCounter
import java.io.InputStream
import java.io.OutputStream
import java.nio.ByteBuffer

/**
 * Created by i_komarov on 12.07.17.
 */

class CustomCommand(
        val customCommand: String,
        val data: ByteArray = ByteArray(0)
): IMessagePayload {

    companion object {
        val serialVersionUID: Long = -8932056829480326011L
        val errorCommand = "ERROR"
    }

    override fun write(output: OutputStream) {
        encoding.varString(customCommand, output)
        output.write(data)
    }

    override fun write(output: ByteBuffer) {
        encoding.varString(customCommand, output)
        output.put(data)
    }

    override fun command(): Command = Command.CUSTOM

    fun isError(): Boolean = customCommand == errorCommand

    class Factory {

        fun create(input: InputStream, length: Int): CustomCommand = with(AccessCounter()) {
            CustomCommand(decoding.varString(input, this), decoding.bytes(input, length - this.length))
        }

        fun createError(message: String): CustomCommand = CustomCommand(errorCommand, message.toByteArray(charset("utf-8")))
    }
}
