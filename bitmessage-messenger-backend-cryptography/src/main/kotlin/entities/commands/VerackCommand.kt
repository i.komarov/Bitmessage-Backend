package entities.commands

import entities.messages.base.IMessagePayload
import entities.messages.base.Command
import java.io.OutputStream
import java.nio.ByteBuffer

/**
 * Created by i_komarov on 11.07.17.
 */

/**
 * The 'verack' customCommand answers a 'version' customCommand, accepting the other node's version.
 */
class VerackCommand: IMessagePayload {

    companion object {
        val serialVersionUID: Long = -4302074845199181687L
    }

    override fun write(output: OutputStream) {
        // 'verack' doesn't have any payload, so there is nothing to write
    }

    override fun write(output: ByteBuffer) {
        // 'verack' doesn't have any payload, so there is nothing to write
    }

    override fun command(): Command = Command.VERACK

}