package entities.encryption

import entities.commands.ObjectCommand
import entities.extra.CryptographyBox
import entities.keys.PublicKeyV3
import entities.keys.PublicKeyV4
import entities.messages.impl.BroadcastMessage
import entities.messages.impl.Message
import entities.messages.plain_text.MessageType
import entities.messages.plain_text.PlaintextMessage

/**
 * Created by i_komarov on 12.07.17.
 */
class DecryptionVisitor(
        private val keyFactory: PublicKeyV3.Factory,
        private val boxManager: CryptographyBox.Manager,
        private val plaintextFactory: PlaintextMessage.Factory
) : IDecryptionVisitor {

    override fun visit(element: ObjectCommand, privateKey: ByteArray) = with(element) {
        if(payload is IEncryptableElement) {
            payload.acceptDecryptionVisitor(this@DecryptionVisitor, privateKey)
        }
    }

    override fun visit(element: PublicKeyV4, privateKey: ByteArray) {
        val encrypted = element.encrypted
        encrypted?.let {
            element.decrypted = keyFactory.create(boxManager.decrypt(it, privateKey), element.streamNumber)
        }
    }

    override fun visit(element: BroadcastMessage, privateKey: ByteArray) {
        element.encrypted?.let {
            element.plaintextMessage = plaintextFactory.create(
                    input = boxManager.decrypt(it, privateKey),
                    messageType = MessageType.BROADCAST
            )
        }
    }

    override fun visit(element: Message, privateKey: ByteArray) {
        element.encrypted?.let {
            element.plaintextMessage = plaintextFactory.create(
                    input = boxManager.decrypt(it, privateKey),
                    messageType = MessageType.MESSAGE
            )
        }
    }
}

