package entities.encryption

import entities.commands.ObjectCommand
import entities.keys.PublicKeyV4
import entities.messages.impl.BroadcastMessage
import entities.messages.impl.Message

/**
 * Created by i_komarov on 12.07.17.
 */
interface IEncryptionVisitor {

    fun visit(element: PublicKeyV4, publicKey: ByteArray)

    fun visit(element: ObjectCommand, publicKey: ByteArray)

    fun visit(element: BroadcastMessage, publicKey: ByteArray)

    fun visit(element: Message, publicKey: ByteArray)
}

interface IDecryptionVisitor {

    fun visit(element: PublicKeyV4, privateKey: ByteArray)

    fun visit(element: ObjectCommand, privateKey: ByteArray)

    fun visit(element: BroadcastMessage, privateKey: ByteArray)

    fun visit(element: Message, privateKey: ByteArray)
}

interface IEncryptableElement {

    fun isDecrypted(): Boolean

    fun acceptEncryptionVisitor(visitor: IEncryptionVisitor, publicKey: ByteArray)

    fun acceptDecryptionVisitor(visitor: IDecryptionVisitor, privateKey: ByteArray)
}