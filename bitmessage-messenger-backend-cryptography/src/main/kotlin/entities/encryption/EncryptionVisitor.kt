package entities.encryption

import engine.base.ICryptographyEngine
import entities.commands.ObjectCommand
import entities.extra.CryptographyBox
import entities.keys.PublicKeyV4
import entities.messages.impl.BroadcastMessage
import entities.messages.impl.Message
import entities.messages.plain_text.PlaintextMessage

/**
 * Created by i_komarov on 12.07.17.
 */
class EncryptionVisitor(
        private val engine: ICryptographyEngine,
        private val factory: CryptographyBox.Factory
): IEncryptionVisitor {

    override fun visit(element: ObjectCommand, publicKey: ByteArray) = with(element) {
        if(payload is IEncryptableElement) {
            payload.acceptEncryptionVisitor(this@EncryptionVisitor, publicKey)
        }
    }

    override fun visit(element: PublicKeyV4, publicKey: ByteArray) {
        if(element.signature() == null) {
            throw IllegalStateException("Public key must be signed before encryption")
        }
        val decrypted = element.decrypted
        decrypted?.let {
            element.encrypted = factory.create(decrypted, publicKey)
        }
    }

    override fun visit(element: BroadcastMessage, publicKey: ByteArray) {
        element.encrypted = element.plaintextMessage?.let {
            factory.create(it, generatePublicKeyIfNeeded(it, publicKey))
        }
    }

    override fun visit(element: Message, publicKey: ByteArray) {
        element.encrypted = element.plaintextMessage?.let { factory.create(it, publicKey) }
    }

    private fun generatePublicKeyIfNeeded(plaintextMessage: PlaintextMessage, publicKey: ByteArray): ByteArray = when(publicKey.size) {
        //generate key if needed
        0 -> engine.generatePublicKeyBytes(plaintextMessage.senderAddress.publicDecryptionKey)
        //if key is already generated just return it
        else -> publicKey
    }
}