package decoding

/**
 * Created by i_komarov on 11.07.17.
 */
class AddressFormatException(message: String): IllegalStateException(message)