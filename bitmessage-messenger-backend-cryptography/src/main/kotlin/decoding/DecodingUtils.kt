package decoding

import cores.base.ICryptographyCore
import statistics.AccessCounter
import utils.base58_indexes
import utils.divmod
import java.io.InputStream
import java.math.BigInteger
import java.nio.ByteBuffer
import java.util.*

/**
 * Created by i_komarov on 05.07.17.
 */

/**
 * This filecontains functions that handle decoding simple types from byte streamNumber, according to
 * https://bitmessage.org/wiki/Protocol_specification#Common_structures
 */

fun varIntList(input: InputStream): LongArray = with(varInt(input).toInt()) {
    LongArray(this, { varInt(input) })
}

fun shortVarBytes(input: InputStream, counter: AccessCounter? = null): ByteArray = with(uint16(input, counter)) {
    bytes(input, this, counter)
}

fun varBytes(input: InputStream, counter: AccessCounter? = null): ByteArray = with(varInt(input, counter).toInt()) {
    bytes(input, this, counter)
}

fun varInt(input: InputStream, counter: AccessCounter? = null): Long = with(input.read()) {
    counter?.increment(1)
    when(this) {
        0xFD -> uint16(input, counter).toLong()
        0xFE -> uint32(input, counter)
        0xFF -> int64(input, counter)
        else -> this.toLong()
    }
}

fun uint8(input: InputStream): Int = input.read()

fun uint16(input: InputStream, counter: AccessCounter? = null): Int {
    counter?.increment(2)
    return input.read() or 8 shl input.read()
}

fun int32(input: InputStream, counter: AccessCounter? = null): Int {
    counter?.increment(4)
    return ByteBuffer.wrap(bytes(input, 4)).getInt()
}

fun uint32(buffer: ByteBuffer): Long = (
        unsigned(buffer.get()) shl 24 or
                unsigned(buffer.get()) shl 16 or
                unsigned(buffer.get()) shl 8 or
                unsigned(buffer.get())
        ).toLong()

fun uint32(input: InputStream, counter: AccessCounter? = null): Long {
    counter?.increment(4)
    return (
            input.read() shl 24 or
            input.read() shl 16 or
            input.read() shl 8 or
            input.read()
    ).toLong()
}

fun int64(input: InputStream, counter: AccessCounter? = null): Long {
    counter?.increment(8)
    return ByteBuffer.wrap(bytes(input, 8)).getLong()
}

fun varString(input: InputStream, counter: AccessCounter?): String {
    val length: Int = varInt(input, counter).toInt()
    val bytes = bytes(input, length, counter)
    return String(bytes, charset("utf-8"))
}

fun bytes(input: InputStream, count: Int, counter: AccessCounter? = null): ByteArray = with(ByteArray(count)) {
    var offset = 0
    while(offset < count) {
        val read = input.read(this, offset, count - offset)
        offset += read
    }
    counter?.increment(count)
    this
}

fun base58_decodeToBigInteger(input: String): BigInteger = BigInteger(
        1, base58_decode(input)
)

fun base58_decodeChecked(core: ICryptographyCore, input: String): ByteArray {
    val decoded: ByteArray = base58_decode(input)
    if(decoded.size < 4) throw AddressFormatException("Input length was ${decoded.size}, required 4 at least")
    val data: ByteArray = Arrays.copyOfRange(decoded, 0, decoded.size - 4)
    val checksum: ByteArray = Arrays.copyOfRange(decoded, decoded.size - 4, decoded.size)
    val actualChecksum: ByteArray = Arrays.copyOfRange(core.doubleSha512(data), 0, 4)
    if(!Arrays.equals(checksum, actualChecksum)) throw AddressFormatException("Checksum validation failed")
    return data
}

fun base58_decode(input: String): ByteArray = when {
    input.isEmpty() -> ByteArray(0)
    else -> {
        val input58: ByteArray = ByteArray(input.length)
        input.forEachIndexed { index, c ->
            val digit: Int = when {
                c.toInt() < 128 -> base58_indexes[c.toInt()]
                else -> -1
            }
            if(digit < 0) throw AddressFormatException("Illegal character $c at position $index")
            input58[index] = digit.toByte()
        }

        var zeros: Int = 0
        while(zeros < input.length && input58[zeros].toInt() == 0) {
            ++zeros
        }

        val decoded: ByteArray = ByteArray(input.length)
        var outputStart = decoded.size
        var inputStart = zeros
        while (inputStart < input58.size) {
            decoded[--outputStart] = divmod(input58, inputStart, 58, 256)
            if (input58[inputStart] == 0.toByte()) {
                ++inputStart // optimization - skip leading zeros
            }
        }

        while(outputStart < decoded.size && decoded[outputStart] == 0.toByte()) {
            ++outputStart
        }

        Arrays.copyOfRange(decoded, outputStart - zeros, decoded.size)
    }
}

private fun unsigned(byte: Byte): Int = byte.toInt() and 0xFF