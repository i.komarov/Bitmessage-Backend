package engine.impl

import engine.base.ICryptographyEngine
import entities.keys.Factory
import entities.keys.PrivateKey
import entities.keys.PublicKey
import entities.messages.base.Feature
import org.spongycastle.asn1.x9.X9ECParameters
import org.spongycastle.crypto.engines.AESEngine
import org.spongycastle.crypto.modes.CBCBlockCipher
import org.spongycastle.crypto.paddings.PKCS7Padding
import org.spongycastle.crypto.paddings.PaddedBufferedBlockCipher
import org.spongycastle.crypto.params.KeyParameter
import org.spongycastle.crypto.params.ParametersWithIV
import org.spongycastle.jce.provider.BouncyCastleProvider
import org.spongycastle.jce.spec.ECParameterSpec
import org.spongycastle.jce.spec.ECPrivateKeySpec
import org.spongycastle.jce.spec.ECPublicKeySpec
import org.spongycastle.math.ec.ECPoint
import utils.keyToBigInteger
import java.math.BigInteger
import java.security.KeyFactory
import java.security.Signature
import java.util.*

/**
 * Created by i_komarov on 05.07.17.
 */

class SpongyCryptographyEngine(private val params: X9ECParameters, private val provider: BouncyCastleProvider, private val factory: Factory): ICryptographyEngine {
    override fun encrypt(data: ByteArray, ellipticCurveKey: ByteArray, initializationVector: ByteArray): ByteArray = with(PaddedBufferedBlockCipher(CBCBlockCipher(AESEngine()), PKCS7Padding())) {
        ParametersWithIV(KeyParameter(ellipticCurveKey), initializationVector).let {
            init(true, it)
            val buffer = ByteArray(getOutputSize(data.size))
            var length = processBytes(data, 0, data.size, buffer, 0)
            length += doFinal(buffer, length)
            when {
                length < buffer.size -> Arrays.copyOfRange(buffer, 0, length)
                else -> buffer
            }
        }
    }

    override fun decrypt(data: ByteArray, ellipticCurveKey: ByteArray, initializationVector: ByteArray): ByteArray = with(PaddedBufferedBlockCipher(CBCBlockCipher(AESEngine()), PKCS7Padding())) {
        ParametersWithIV(KeyParameter(ellipticCurveKey), initializationVector).let {
            init(false, it)
            val buffer = ByteArray(getOutputSize(data.size))
            var length = processBytes(data, 0, data.size, buffer, 0)
            length += doFinal(buffer, length)
            when {
                length < buffer.size -> Arrays.copyOfRange(buffer, 0, length)
                else -> buffer
            }
        }
    }

    override fun generatePublicKey(version: Long, streamNumber: Long, privateSigningKey: ByteArray, privateEncryptionKey: ByteArray,
                                   nonceTrialsPerByte: Long, extraBytes: Long, vararg features: Feature): PublicKey
            = factory.generatePublicKey(
            version, streamNumber, privateSigningKey, privateEncryptionKey,
            nonceTrialsPerByte, extraBytes, *features
    )

    override fun generatePublicKeyBytes(privateKeyBytes: ByteArray): ByteArray = params
            .g.multiply(keyToBigInteger(privateKeyBytes))
            .normalize()
            .getEncoded(false)

    override fun ensureSignatureValidity(data: ByteArray, signature: ByteArray, publicKey: PublicKey): Boolean = with(
            ECParameterSpec(params.getCurve(), params.getG(), params.getN(), params.getH(), params.getSeed())) {
        val point = keyToEllipticCurvePoint(publicKey.signingKey())
        val spec = ECPublicKeySpec(point, this)
        return Signature.getInstance("ECDSA", provider).apply {
            initVerify(KeyFactory.getInstance("ECDSA", provider).generatePublic(spec))
            update(data)
        }.verify(signature)
    }

    override fun generateSignature(data: ByteArray, privateKey: PrivateKey): ByteArray = with(
            ECParameterSpec(params.getCurve(), params.getG(), params.getN(), params.getH(), params.getSeed())) {
        val d = keyToBigInteger(privateKey.privateSigningKey)
        val keySpec = ECPrivateKeySpec(d, this)
        val javaPrivateKey = KeyFactory.getInstance("ECDSA", provider).generatePrivate(keySpec)
        return Signature.getInstance("ECDSA", provider).apply {
            initSign(javaPrivateKey)
            update(data)
        }.sign()
    }

    private fun keyToEllipticCurvePoint(publicKey: ByteArray): ECPoint = params.curve.createPoint(
            BigInteger(1, Arrays.copyOfRange(publicKey, 1, 33)),
            BigInteger(1, Arrays.copyOfRange(publicKey, 33, 65))
    )
}