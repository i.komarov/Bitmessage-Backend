package engine.impl

import engine.base.ICryptographyEngine
import entities.keys.Factory
import entities.keys.PrivateKey
import entities.keys.PublicKey
import entities.messages.base.Feature

import org.bouncycastle.asn1.x9.X9ECParameters
import org.bouncycastle.crypto.engines.AESEngine
import org.bouncycastle.crypto.modes.CBCBlockCipher
import org.bouncycastle.crypto.paddings.PKCS7Padding
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher
import org.bouncycastle.crypto.params.KeyParameter
import org.bouncycastle.crypto.params.ParametersWithIV
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.bouncycastle.jce.spec.ECParameterSpec
import org.bouncycastle.jce.spec.ECPrivateKeySpec
import org.bouncycastle.jce.spec.ECPublicKeySpec
import org.bouncycastle.math.ec.ECPoint
import utils.keyToBigInteger
import java.math.BigInteger
import java.security.KeyFactory
import java.security.Signature
import java.util.*

/**
 * Created by i_komarov on 05.07.17.
 */
class BouncyCryptographyEngine(private val params: X9ECParameters, private val provider: BouncyCastleProvider, private val factory: Factory): ICryptographyEngine {

    override fun encrypt(data: ByteArray, ellipticCurveKey: ByteArray, initializationVector: ByteArray): ByteArray = with(PaddedBufferedBlockCipher(CBCBlockCipher(AESEngine()), PKCS7Padding())) {
        ParametersWithIV(KeyParameter(ellipticCurveKey), initializationVector).let {
            init(true, it)
            val buffer = ByteArray(getOutputSize(data.size))
            var length = processBytes(data, 0, data.size, buffer, 0)
            length += doFinal(buffer, length)
            when {
                length < buffer.size -> Arrays.copyOfRange(buffer, 0, length)
                else -> buffer
            }
        }
    }

    override fun decrypt(data: ByteArray, ellipticCurveKey: ByteArray, initializationVector: ByteArray): ByteArray = with(PaddedBufferedBlockCipher(CBCBlockCipher(AESEngine()), PKCS7Padding())) {
        ParametersWithIV(KeyParameter(ellipticCurveKey), initializationVector).let {
            init(false, it)
            val buffer = ByteArray(getOutputSize(data.size))
            var length = processBytes(data, 0, data.size, buffer, 0)
            length += doFinal(buffer, length)
            when {
                length < buffer.size -> Arrays.copyOfRange(buffer, 0, length)
                else -> buffer
            }
        }
    }

    override fun generatePublicKey(version: Long, streamNumber: Long, privateSigningKey: ByteArray, privateEncryptionKey: ByteArray,
                                   nonceTrialsPerByte: Long, extraBytes: Long, vararg features: Feature): PublicKey
            = factory.generatePublicKey(
            version, streamNumber, privateSigningKey, privateEncryptionKey,
            nonceTrialsPerByte, extraBytes, *features
    )

    override fun generatePublicKeyBytes(privateKeyBytes: ByteArray): ByteArray = params
            .g.multiply(keyToBigInteger(privateKeyBytes))
            .normalize()
            .getEncoded(false)

    override fun ensureSignatureValidity(data: ByteArray, signature: ByteArray, publicKey: PublicKey): Boolean = with(
            ECParameterSpec(params.getCurve(), params.getG(), params.getN(), params.getH(), params.getSeed())) {
        val point = keyToEllipticCurvePoint(publicKey.signingKey())
        val spec = ECPublicKeySpec(point, this)
        return Signature.getInstance("ECDSA", provider).apply {
            initVerify(KeyFactory.getInstance("ECDSA", provider).generatePublic(spec))
            update(data)
        }.verify(signature)
    }

    override fun generateSignature(data: ByteArray, privateKey: PrivateKey): ByteArray = with(
            ECParameterSpec(params.getCurve(), params.getG(), params.getN(), params.getH(), params.getSeed())) {
        val d = keyToBigInteger(privateKey.privateSigningKey)
        val keySpec = ECPrivateKeySpec(d, this)
        val javaPrivateKey = KeyFactory.getInstance("ECDSA", provider).generatePrivate(keySpec)
        return Signature.getInstance("ECDSA", provider).apply {
            initSign(javaPrivateKey)
            update(data)
        }.sign()
    }

    private fun keyToEllipticCurvePoint(publicKey: ByteArray): ECPoint = params.curve.createPoint(
            BigInteger(1, Arrays.copyOfRange(publicKey, 1, 33)),
            BigInteger(1, Arrays.copyOfRange(publicKey, 33, 65))
    )
}