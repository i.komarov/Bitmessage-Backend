package engine.base

import entities.keys.PrivateKey
import entities.keys.PublicKey
import entities.messages.base.Feature

/**
 * Created by i_komarov on 05.07.17.
 */
interface ICryptographyEngine {

    fun encrypt(data: ByteArray, ellipticCurveKey: ByteArray, initializationVector: ByteArray): ByteArray

    fun decrypt(data: ByteArray, ellipticCurveKey: ByteArray, initializationVector: ByteArray): ByteArray

    /**
     * Create a new public key fom given private entities.keys.
     *
     * @param version              of the public key / entities.address
     * @param stream               of the entities.address
     * @param privateSigningKey    private key used for signing
     * @param privateEncryptionKey private key used for encryption
     * @param nonceTrialsPerByte   proof of work difficulty
     * @param extraBytes           bytes to add for the proof of work (make it harder for small messages)
     * @param features             of the entities.address
     * @return a public key object
     */
    fun generatePublicKey(
            version: Long,
            streamNumber: Long,
            privateSigningKey: ByteArray,
            privateEncryptionKey: ByteArray,
            nonceTrialsPerByte: Long,
            extraBytes: Long,
            vararg features: Feature
     ): PublicKey

    /**
     * @param privateKeyBytes private key byte array representation
     * @return a public key corresponding to the given private key
     */
    fun generatePublicKeyBytes(privateKeyBytes: ByteArray): ByteArray

    /**
     * @param data      to check
     * @param signature the signature of the entities.messages
     * @param publicKey    the sender's public key
     * @return boolean representing validity of signature
     */
    fun ensureSignatureValidity(data: ByteArray, signature: ByteArray, publicKey: PublicKey): Boolean

    /**
     * Calculate the signature of data, using the given private key.
     *
     * @param data       to be signed
     * @param privateKey to be used for signing
     * @return the signature
     */
    fun generateSignature(data: ByteArray, privateKey: PrivateKey): ByteArray
}