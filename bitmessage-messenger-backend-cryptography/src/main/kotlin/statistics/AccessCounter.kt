package statistics

/**
 * Created by i_komarov on 04.07.17.
 */

class AccessCounter {

    companion object {
        fun increment(counter: AccessCounter, length: Int = 1) = counter.increment(length)
    }

    var length: Int = 0
        private set

    fun increment(length: Int) {
        this.length += length
    }

    override fun toString(): String {
        return length.toString()
    }
}