package base

import java.io.OutputStream
import java.nio.ByteBuffer

/**
 * Created by i_komarov on 04.07.17.
 */
interface Streamable {

    fun write(output: OutputStream)

    fun write(output: ByteBuffer)
}