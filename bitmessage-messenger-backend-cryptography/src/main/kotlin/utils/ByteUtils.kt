package utils

/**
 * Created by i_komarov on 04.07.17.
 */

fun truncate(source: ByteArray, size: Int): ByteArray = with(ByteArray(size)) {
    System.arraycopy(source, 0, this, size - source.size, source.size)
    this
}

fun leadingZeros(bytes: ByteArray): Int = (1..bytes.size).firstOrNull { bytes[it - 1] != 0.toByte() } ?: bytes.size

fun expand(source: ByteArray, size: Int): ByteArray = with(ByteArray(size)) {
    System.arraycopy(source, 0, this, size - source.size, source.size)
    this
}

fun inc(nonce: ByteArray) {
    for(i in nonce.size - 1 downTo 0) {
        nonce[i] ++
        if(nonce[i] != 0.toByte()) {
            break
        }
    }
}

fun inc(nonce: ByteArray, value: Byte) {
    val i: Int = nonce.size - 1
    nonce[i] = (nonce[i] + value).toByte()
    if((value > 0 && (nonce[i] < 0 || nonce[i] >= value)) || (value < 0 && (nonce[i] < 0 && nonce[i] >= value))) return;

    for(j in i - 1 downTo 0) {
        nonce[j] ++
        if(nonce[j] != 0.toByte()) {
            break;
        }
    }
}