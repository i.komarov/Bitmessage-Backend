package utils

/**
 * Created by i_komarov on 11.07.17.
 */

fun toHexString(bytes: ByteArray): StringBuilder = with(StringBuilder()) {
    bytes.forEach { append(String.format("%02x", it)) }
    this
}

fun toCommaSeparatedString(vararg objects: Any): StringBuilder = with(StringBuilder()) {
    objects.forEachIndexed { index, any ->
        if(index != 0) append(",")
        append(any)
    }
    this
}