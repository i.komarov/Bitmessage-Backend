package utils

import java.math.BigInteger
import java.util.*

/**
 * Created by i_komarov on 05.07.17.
 */

fun keyToBigInteger(publicKey: ByteArray): BigInteger = BigInteger(1, publicKey)

fun componentX(point: ByteArray): ByteArray = Arrays.copyOfRange(point, 1, ((point.size - 1) / 2) + 1)

fun componentY(point: ByteArray): ByteArray = Arrays.copyOfRange(point, ((point.size - 1) / 2) + 1, point.size)