package utils

/**
 * Created by i_komarov on 11.07.17.
 */

val base58_alphabet: CharArray by lazy { "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz".toCharArray() }
val base58_encodedZero: Char by lazy { base58_alphabet[0] }
val base58_indexes: IntArray by lazy {
    val array = IntArray(128, { -1 })
    for(it in 0..base58_alphabet.size - 1) {
        array[base58_alphabet[it].toInt()] = it
    }

    array
}
