package utils

/**
 * Created by i_komarov on 11.07.17.
 */

val MINUTE_SECONDS: Long = 60
val HOUR_SECONDS: Long = 60 * MINUTE_SECONDS
val DAY_SECONDS: Long = 24 * HOUR_SECONDS

private var messageTTL: Long = 2 * DAY_SECONDS
private var getPublicKetMessageTTL: Long = 2 * DAY_SECONDS
private var publicKeyTTL: Long = 28 * DAY_SECONDS

fun unixTime(shiftSeconds: Long = 0L): Long = (System.currentTimeMillis() / 1000) + shiftSeconds

fun messageTTL(): Long = messageTTL

fun messageTTL(ttl: Long) {
    messageTTL = ttl
}

fun getPublicKeyMessageTTL(): Long = getPublicKetMessageTTL

fun getPublicKeyMessageTTL(ttl: Long) {
    getPublicKetMessageTTL = ttl
}

fun publicKeyTTL(): Long = publicKeyTTL

fun publicKeyTTL(ttl: Long) {
    publicKeyTTL = ttl
}
