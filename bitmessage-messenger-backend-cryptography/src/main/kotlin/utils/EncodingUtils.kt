package utils

import kotlin.experimental.and

/**
 * Created by i_komarov on 11.07.17.
 */

fun divmod58(number: ByteArray, start: Int): Byte {
    var remainder: Int = 0
    for(it in start..number.size - 1) {
        val digit256 = (number[it].toInt() and 0xFF)
        val temp = remainder * 256 + digit256
        number[it] = (temp / 58).toByte()
        remainder = temp.rem(58)
    }

    return remainder.toByte()
}

fun divmod256(number58: ByteArray, start: Int): Byte {
    var remainder = 0
    for(it in start..number58.size - 1) {
        val digit58 = (number58[it].toInt() and 0xFF)
        val temp = remainder * 58 + digit58
        number58[it] = (temp / 256).toByte()
        remainder = temp.rem(256)
    }

    return remainder.toByte()
}

fun divmod(number: ByteArray, firstDigit: Int, base: Int, divisor: Int): Byte {
    var remainder = 0
    for(it in firstDigit..number.size - 1) {
        val digit = number[it].toInt() and 0xFF
        val temp = remainder * base + digit
        number[it] = (temp / divisor).toByte()
        remainder = temp.rem(divisor)
    }

    return remainder.toByte()
}