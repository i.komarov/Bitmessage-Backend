package constants

/**
 * Created by i_komarov on 11.07.17.
 */

val CURRENT_VERSION = 4
val SUPPORTED_VERSIONS = longArrayOf(1, 2, 3, 4)
val USER_AGENT: String = "/ikomarov:bitmessage:1.0.0/"

val ACKNOWLEDGEMENT_SIZE = 32