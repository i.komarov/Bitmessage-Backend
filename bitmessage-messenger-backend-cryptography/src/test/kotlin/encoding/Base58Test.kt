package encoding

import decoding.base58_decode
import org.junit.Test
import java.math.BigInteger
import java.util.*
import kotlin.test.assertEquals
import kotlin.test.assertTrue

/**
 * Created by i_komarov on 11.07.17.
 */
class Base58Test {

    @Test
    fun test_base58_encode() {
        val testbytes = "Hello World".toByteArray()
        assertEquals("JxF12TrwUP45BMd", base58_encode(testbytes))

        val bi = BigInteger.valueOf(3471844090L)
        assertEquals("16Ho7Hs", base58_encode(bi.toByteArray()))

        val zeroBytes1 = ByteArray(1)
        assertEquals("1", base58_encode(zeroBytes1))

        val zeroBytes7 = ByteArray(7)
        assertEquals("1111111", base58_encode(zeroBytes7))

        assertEquals("", base58_encode(ByteArray(0)))
    }
}