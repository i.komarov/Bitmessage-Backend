package decoding

import org.junit.Test
import java.util.*
import kotlin.test.assertTrue

/**
 * Created by i_komarov on 11.07.17.
 */

class Base58Test {

    @Test
    fun test_base58_decode() {
        val testbytes = "Hello World".toByteArray()
        val actualbytes = base58_decode("JxF12TrwUP45BMd")
        assertTrue(String(actualbytes), { Arrays.equals(testbytes, actualbytes) })

        assertTrue("1", { Arrays.equals(base58_decode("1"), ByteArray(1)) })
        assertTrue("1111", { Arrays.equals(base58_decode("1111"), ByteArray(4)) })
    }
}